package rest.letsmeet.org.letsmeetapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import rest.letsmeet.org.letsmeetapp.adapter.ParticipantFragmentPagerAdapter;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.model.Participation;
import rest.letsmeet.org.letsmeetapp.model.ParticipationStatus;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import rest.letsmeet.org.letsmeetapp.tab.EventDetailsFragment_Comments;
import rest.letsmeet.org.letsmeetapp.tab.EventDetailsFragment_Details;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.EVENT_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.EVENT_ORGANIZER_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;

public class EventDetailsActivity extends BaseActivity {

    private long userID;
    private long eventID;
    private long organizerId;
    private Context context;
    private Bundle extras;

    private TabLayout tabLayout;
    FrameLayout frameLayot;

    private ViewPager viewPager_users;
    private BottomNavigationView bottomNavigation;
    FloatingActionButton fab_plus, fab_walk;

    Dialog dialog;
    Button cancel_btn, confirm_btn;
    TextView title, description;

    Fragment fragment;
    android.support.v4.app.FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        extras = getIntent().getExtras();

        if(extras != null){
            eventID = extras.getLong(EVENT_ID);
            userID = extras.getLong(USER_ID);
            organizerId = extras.getLong(EVENT_ORGANIZER_ID);
        }else{
           // Toast.makeText(context, "Bundle is null", Toast.LENGTH_SHORT).show();
        }
        //setContentView(R.layout.event_details_tab_layout);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.event_details_tab_layout, contentFrameLayout);
       // putAdditionalExtras(eventID, userID);
        setToolbar();
        setFabs();
        setTabLayout();


        /*bottom navigation*/
        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        //initial setting
        fragment = new EventDetailsFragment_Details();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
        frameLayot = (FrameLayout) findViewById(R.id.fragmentContainer);

        bottomNavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.bottom_navigation_details:
                                viewPager_users.setVisibility(View.INVISIBLE);
                                frameLayot.setVisibility(View.VISIBLE);
                                tabLayout.setVisibility(View.INVISIBLE);

                                fab_walk.setVisibility(View.VISIBLE);
                                fab_plus.setVisibility(View.INVISIBLE);
                                fab_walk.setClickable(true);
                                fab_plus.setClickable(false);

                                fragment = new EventDetailsFragment_Details();
                                fragmentManager = getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
                                break;
                            case R.id.bottom_navigation_comments:
                                viewPager_users.setVisibility(View.INVISIBLE);
                                frameLayot.setVisibility(View.VISIBLE);
                                tabLayout.setVisibility(View.INVISIBLE);

                                fab_walk.setVisibility(View.INVISIBLE);
                                fab_plus.setVisibility(View.INVISIBLE);
                                fab_walk.setClickable(false);
                                fab_plus.setClickable(false);
                                fragment = new EventDetailsFragment_Comments();
                                fragmentManager = getSupportFragmentManager();
                                fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
                                break;
                            case R.id.bottom_navigation_users:
                                viewPager_users.setVisibility(View.VISIBLE);
                                frameLayot.setVisibility(View.INVISIBLE);
                                tabLayout.setVisibility(View.VISIBLE);

                                fab_walk.setVisibility(View.INVISIBLE);
                                fab_plus.setVisibility(View.VISIBLE);
                                fab_walk.setClickable(false);
                                fab_plus.setClickable(true);
                               // fragment = new EventDetailsFragment_Users();
                               // fragmentManager = getSupportFragmentManager();
                               // fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
                                break;
                            default:
                                break;
                        }
                        return true;
                    }
                });

    }

    private void setFabs() {
        fab_plus = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_walk = (FloatingActionButton) findViewById(R.id.fab_walk);

        fab_walk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.event_walk_dialog);
                cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
                confirm_btn = (Button) dialog.findViewById(R.id.confirm_btn);
                description = (TextView) dialog.findViewById(R.id.descr_tv);
                title = (TextView) dialog.findViewById(R.id.title_tv);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                if(organizerId == userID){
                    title.setText("Jesteś organizatorem!");
                    description.setText("Nie możesz się wycofać.");
                    confirm_btn.setText("ROZUMIEM");
                    cancel_btn.setVisibility(View.INVISIBLE);
                    cancel_btn.setClickable(false);

                    confirm_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                }
                else{
                    confirm_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setOrUnsetParticipation(userID, eventID);
                            dialog.dismiss();
                        }
                    });


                    try {
                        APIService service = ApiClient.getRetrofit().create(APIService.class);
                        Call<Participation> call = service.getParticipation(userID, eventID);
                        //  Log.d(String.valueOf(Log.INFO), service.getGroups().request().url().toString());

                        call.enqueue(new Callback<Participation>() {
                            @Override
                            public void onResponse(Call<Participation> call, Response<Participation> response) {
                                if (response.code() == 200) {
                                    Participation participation = response.body();

                                    cancel_btn.setVisibility(View.VISIBLE);
                                    confirm_btn.setClickable(true);
                                    confirm_btn.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));

                                    if (participation.getId() == 0) {
                                        title.setText("Czy na pewno chcesz dołączyć do wydarzenia?");
                                        description.setText("Twoje zgłoszenie będzie oczekiwało na akceptację.");
                                        confirm_btn.setText(context.getString(R.string.btn_text_join));

                                    } else if (participation.getStatus().getName().equals("pending") && participation.haveThisUserMadeRecentChange()) {
                                        //cancell
                                        title.setText("Czy na pewno chcesz opuścić wydarzenie?");
                                        description.setText("Jeśli to zrobisz Twoje zgłoszenie zostanie wycofane.");
                                        confirm_btn.setText(context.getString(R.string.btn_text_leave));
                                    } else if (participation.getStatus().getName().equals("pending") && !participation.haveThisUserMadeRecentChange()) {
                                        title.setText("Czy na pewno chcesz dołączyć?");
                                        description.setText("Jeśli to zrobisz Twoje uczestnictwo zostanie automatycznie zaakceptowane - ktoś zaprosił Cię na to wydarzenie.");
                                        confirm_btn.setText(context.getString(R.string.btn_text_join));
                                    } else if (participation.getStatus().getName().equals("accepted")) {
                                        title.setText("Czy na pewno chcesz opuścić wydarzenie?");
                                        description.setText("Jeśli opuścisz wydarzenie będziesz musiał ponownie wysłać prośbę o dołączenie i czekać na jej akceptację przez organizatora wydarzenia.");
                                        confirm_btn.setText(context.getString(R.string.btn_text_leave));
                                    } else if (participation.getStatus().getName().equals("declined")) {
                                        title.setText("Brak możliwych akcji do wykonania");
                                        description.setText("Organizator wydarzenia odrzucił Twoje zgłosznie. Nie możesz wysłać go ponownie. ");
                                        confirm_btn.setText(context.getString(R.string.btn_text_join));
                                        confirm_btn.setClickable(false);
                                        confirm_btn.setTextColor(context.getResources().getColor(R.color.inactiveElement));
                                    } else if (participation.getStatus().getName().equals("cancelled")) {
                                        title.setText("Czy na pewno chcesz dołączyć?");
                                        description.setText("Twoje zgłoszenie będzie oczekiwało na akceptację.");
                                        confirm_btn.setText(context.getString(R.string.btn_text_join));
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<Participation> call, Throwable t) {
                              //  Toast.makeText(context, "onFailure - getParticipation", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } catch (Exception e) {
                        Log.e("Error in fetching data", e.getMessage());
                    }
                }

                dialog.show();
            }
        });
    }

    private void setTabLayout() {
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
       // viewPager_det = (ViewPager) findViewById(R.id.viewpager_details);
        //viewPager_comm = (ViewPager) findViewById(R.id.viewpager_comments);
        viewPager_users = (ViewPager) findViewById(R.id.viewpager);
        ParticipantFragmentPagerAdapter adapter_users = new ParticipantFragmentPagerAdapter(this, getSupportFragmentManager(), extras);
        viewPager_users.setAdapter(adapter_users);

        tabLayout.setupWithViewPager(viewPager_users);
    }

    private void setToolbar() {

        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            final Call<Event> call = service.getEvent(eventID);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<Event>() {
                @Override
                public void onResponse(Call<Event> call, Response<Event> response) {
                    if(response.code() == 200){
                        if(response.body() != null){
                            Event event = response.body();
                            toolbar.setTitle(event.getName());
                        }
                    }
                }
                @Override
                public void onFailure(Call<Event> call, Throwable t) {
                   // Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error in getEvent",e.getMessage());
        }
    }

    private void setOrUnsetParticipation(long userId, long eventId) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<ParticipationStatus> call = service.setParticipation(eventId, userId);

            call.enqueue(new Callback<ParticipationStatus>() {
                @Override
                public void onResponse(Call<ParticipationStatus> call, Response<ParticipationStatus> response) {
                    if(response.code() == 200){
                       // Toast.makeText(context, "currentStatus: " +response.body().getName(), Toast.LENGTH_SHORT).show();
                    }
                }
                @Override
                public void onFailure(Call<ParticipationStatus> call, Throwable t) {
                   // Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error in fetching cat.",e.getMessage());
        }
    }
}
