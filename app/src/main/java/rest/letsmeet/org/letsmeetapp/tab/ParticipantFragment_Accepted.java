package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_ACCEPTED;

public class ParticipantFragment_Accepted extends ParticipantFragment {

    public ParticipantFragment_Accepted(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = PARTICIPATION_ACCEPTED;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

}
