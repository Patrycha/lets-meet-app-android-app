package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.EventsTabsActivity;
import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.Group;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GroupRVAdapter extends RecyclerView.Adapter<GroupRVAdapter.GroupViewHolder>{
    private long userID;
    private boolean areAdminGroups;

    List<Group> groups;
    Context context;

    public GroupRVAdapter(Context context, List<Group> groups, long userId, boolean areAdminGroups){
        this.context = context;
        userID = userId;
        this.areAdminGroups = areAdminGroups;

        if(groups != null) this.groups = groups;
        else this.groups = new ArrayList<>();


    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_card, parent, false);
        GroupViewHolder gvh = new GroupViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, final int position) {
        final Group group = groups.get(position);

        holder.groupName.setText(group.getName());

        setUserCounter(holder.userCounter, group.getId(), position);
        setEventCounter(holder.eventCounter, group.getId(), position);

        if(!areAdminGroups){
            if (groups.get(position).getAdminId() == userID){
                setButton(holder.joinButton, group.getId(), userID, position, true);
            }else{
                setButton(holder.joinButton, group.getId(), userID, position, false);
                holder.joinButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        joinOrLeaveGroup(userID, groups.get(position).getId());
                    }
                });
            }
        }else {
            setButton(holder.joinButton, group.getId(), userID, position, areAdminGroups);
        }
//        if(!areAdminGroups) {
//            holder.joinButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    joinOrLeaveGroup(userID, groups.get(position).getId());
//                }
//            });
//        }
        holder.detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = new Bundle();
                extras.putLong("groupID", groups.get(position).getId());
                extras.putLong("userID", userID);
                extras.putLong("groupAdminID", group.getAdminId());
                Intent intent = new Intent(context, EventsTabsActivity.class);
                intent.putExtras(extras);
                context.startActivity(intent);
            }
        });
    }

    private void setEventCounter(final TextView textView, long groupId, final int position) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Integer> call = service.getEventsCount(groupId);

            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    if(response.code() == 200){
                        if(response.body() == null) {
                            textView.setText(context.getResources().getString(R.string.count_not_known));
                        }
                        else {
                            textView.setText(response.body().toString());
                        }
                       // notifyItemChanged(position);
                    }
                }
                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }
    public void setUserCounter(final TextView textView, long groupId, final int position) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Integer> call = service.getSubscribersCount(groupId);

            call.enqueue(new Callback<Integer>() {
                @Override
                public void onResponse(Call<Integer> call, Response<Integer> response) {
                    if(response.code() == 200){
                        if(response.body() == null) {
                            textView.setText(context.getResources().getString(R.string.count_not_known));
                        }
                        else {
                            textView.setText(response.body().intValue() + 1 + "");//plus group administrator
                        }
                   //     notifyItemChanged(position);
                    }
                }
                @Override
                public void onFailure(Call<Integer> call, Throwable t) {
                    Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }
    private void setButton(final Button button, final long groupId, final long userId, final int position, boolean areAdminGroups) {
        if(areAdminGroups){
           button.setText(context.getResources().getString(R.string.btn_text_leave));
            button.setClickable(false);
            button.setTextColor(context.getResources().getColor(R.color.inactiveElement));

        }
        else {
            try {
                final APIService service = ApiClient.getRetrofit().create(APIService.class);
                Call<Boolean> call = service.isSubscribing(userId, groupId);

                call.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        if (response.code() == 200) {
                            if (response.body() == null) {
                                button.setText(context.getResources().getString(R.string.btn_text_join));
                                button.setTextColor(context.getResources().getColor(R.color.inactiveElement));
                                button.setClickable(false);
                            } else if (response.body()) {
                                button.setText(context.getResources().getString(R.string.btn_text_leave));
                                button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                                button.setClickable(true);

                            } else {
                                try {
                                    Call<Boolean> call2 = service.isOrganizer(userId, groupId);
                                    call2.enqueue(new Callback<Boolean>() {
                                        @Override
                                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                                            if (response.code() == 200) {
                                                if (response.body() != null) {
                                                    if (response.body()) {
                                                        button.setText(context.getResources().getString(R.string.btn_text_leave));
                                                        button.setTextColor(context.getResources().getColor(R.color.inactiveElement));
                                                        button.setClickable(false);
                                                    } else {
                                                        button.setText(context.getResources().getString(R.string.btn_text_join));
                                                        button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                                                        button.setClickable(true);
                                                    }
                                                }
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Boolean> call, Throwable t) {

                                        }
                                    });
                                } catch (Exception e) {
                                    Log.e("Error", e.getMessage());
                                }

                            }
                            notifyItemChanged(position);
                        }
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
            }
        }
    }

    private void joinOrLeaveGroup(final long userId, final long groupId) {
        //1. check if user is part of the group
        //2. subscribe ot unsubscribe
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Boolean> call = service.isSubscribing(userId, groupId);

            //  Log.d(String.valueOf(Log.INFO), service.getGroups().request().url().toString());

            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if(response.code() == 200){

                        if(!response.body()){
                            setSubscribtion(userId, groupId);
                        }else{
                            unsetSubscribtion(userId, groupId);
                        }
                    }
                }
                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error in fetching cat.",e.getMessage());
        }
    }

    private void setSubscribtion(long userId, long groupId) {
        APIService service = ApiClient.getRetrofit().create(APIService.class);

        service.setSubsciption(userId, groupId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 201){
                    Toast.makeText(context, "Zapisano", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Toast.makeText(context, t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void unsetSubscribtion(long userId, long groupId) {
        APIService service = ApiClient.getRetrofit().create(APIService.class);

        service.unsetSubscribtion(userId, groupId).enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.code() == 200){
                    Toast.makeText(context, "Wypisano", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Toast.makeText(context, t.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return groups.size();
    }


    public static class GroupViewHolder extends RecyclerView.ViewHolder{

        TextView groupName;
        TextView userCounter;
        TextView eventCounter;
        Button joinButton;
        Button detailButton;




        public GroupViewHolder(View itemView) {
            super(itemView);
            groupName = (TextView) itemView.findViewById(R.id.group_name_tv);
            userCounter = (TextView) itemView.findViewById(R.id.users_count_tv);
            eventCounter = (TextView) itemView.findViewById(R.id.events_count_tv);
            joinButton = (Button) itemView.findViewById(R.id.group_join_btn);
            detailButton = (Button) itemView.findViewById(R.id.group_details_btn);
        }
    }
}
