package rest.letsmeet.org.letsmeetapp.busevent;

public class ChangeParticipationStatusEvent {

    private final String message;

    public ChangeParticipationStatusEvent(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
    }
