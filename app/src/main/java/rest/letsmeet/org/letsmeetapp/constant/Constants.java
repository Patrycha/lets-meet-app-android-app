package rest.letsmeet.org.letsmeetapp.constant;

public interface Constants {

    //VisibilityStatus names
    public static final String VISIBILITY_PRIVATE = "private";
    public static final String VISIBILITY_PUBLIC = "public";

    //ParticipationStatus names
    public static final String PARTICIPATION_STATUS = "status";
    public static final String PARTICIPATION_ACCEPTED = "accepted";
    public static final String PARTICIPATION_PENDING = "pending";
    public static final String PARTICIPATION_DECLINED = "declined";
    public static final String PARTICIPATION_INVITED = "invited";
    public static final String NOTIFICATION_CREATED = "created";
    public static final String NOTIFICATION_CANCELLED = "cancelled";

    //SharedPreferences
    public static final String PREF_FILE_NAME = "testpref";
    public static final String USER_ID = "userID";
    public static final String USER_NAME = "userName";
    public static final String USER_SURNAME = "userSurname";
    public static final String USER_LOGIN = "userLogin";
    public static final String USER_IMAGE_PATH = "imagePath";
    public static final String GROUP_ID = "groupID";
    public static final String GROUP_DESCR = "groupDescription";
    public static final String EVENT_ID = "eventID";
    public static final String IS_EVENT_ADMIN = "isEventAdmin";
    public static final String EVENT_ORGANIZER_ID = "eventOrgId";

    //data-time constants
    public static final String YMD = "yyyy-MM-dd";
    public static final String DMY = "dd-MM-yyyy";
    public static final String HM = "HH:mm";



}

