package rest.letsmeet.org.letsmeetapp.adapter;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Or;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import rest.letsmeet.org.letsmeetapp.R;

/**
 * Created by Patrycja on 07.11.2017.
 */

public class NewEventDialog extends Dialog {
    Button cancel_btn, confirm_btn;
    TextView date_bigin;
    ImageButton calendar_btn;

    SeekBar seekbar;
    EditText seekbar_counter;
    TextView seekbar_limit;

    @NotEmpty(message = "Pole obowiązkowe")
    @Or
    @Length(max = 50, message = "Niepoprawna długość")
    EditText name_tv;

    @Length(max = 255, message = "Niepoprawna długość")
    EditText descr_tv;

    @Length(max = 100, message = "Niepoprawna długość")
    EditText place_tv;

    @Max(value = 100, message = "Maksymalnie 100 użytkowników")
    @Or
    @Min(value = 1, message = "Minimum 1 użytkownik")
    EditText user_limit;

    CheckBox visibility_status_chb;

    //save this data
    Date event_start_date;
    Time event_start_time;

    public NewEventDialog(@NonNull Context context) {
        super(context, android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
        this.setContentView(R.layout.full_screen_dialog);

        date_bigin = (TextView) this.findViewById(R.id.newevent_date_begin);
        calendar_btn = (ImageButton) this.findViewById(R.id.newevent_calendar_btn);
        setSeekbar(this);

        final TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), R.style.DateTimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                event_start_time = Time.valueOf(hourOfDay+":"+minute+":"+0);
                CharSequence temp = date_bigin.getText();
                date_bigin.setText(temp + " " + (""+event_start_time).substring(0,5));
            }
        }, 12, 0, true);

        final Calendar calendar = Calendar.getInstance();
        int y = calendar.get(Calendar.YEAR);
        int m = calendar.get(Calendar.MONTH);
        int d = calendar.get(Calendar.DAY_OF_MONTH);

        final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DateTimePickerTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                timePickerDialog.show();

                event_start_date = new Date(year-1900, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                date_bigin.setText(simpleDateFormat.format(event_start_date));

            }
        }, y, m, d);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        calendar_btn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                datePickerDialog.show();
                                            }
                                        }
        );
        //fields
        name_tv = (EditText) this.findViewById(R.id.edit_newevent_name);
        descr_tv = (EditText) this.findViewById(R.id.edit_newevent_descr);
        place_tv = (EditText) this.findViewById(R.id.edit_newevent_place);
        visibility_status_chb = (CheckBox) this.findViewById(R.id.newevent_visibility_checkbox);
        //buttons
        confirm_btn = (Button) this.findViewById(R.id.newevent_create_btn);
        cancel_btn = (Button)this.findViewById(R.id.newevent_cancel_btn);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  EventsTabsActivity.super.onClick(v);
                //validate data
               // String name = name_tv.getText().toString();
                //String descr = descr_tv.getText().toString();
                //String place = place_tv.getText().toString();
                //boolean is_private = visibility_status_chb.isChecked();
                //event_start_date
                //event_start_time
                if (true) {
                    // Our form is successfully validated, so, do your stuffs here...
                    Toast.makeText(getContext(), "Form Successfully Validated", Toast.LENGTH_LONG).show();
                }

                //try to save new event

                //close dialog
                //dialog.dismiss();
            }
        });
    }

    public void setSeekbar(Dialog dialog){
        seekbar = (SeekBar) dialog.findViewById(R.id.newevent_seekbar);
        seekbar_counter = (EditText) dialog.findViewById(R.id.newevent_seekBar_count);
        seekbar_limit = (TextView) dialog.findViewById(R.id.newevent_seekbar_count_max);

        seekbar_counter.setText(seekbar.getProgress()+"");
        seekbar_limit.setText(" / " + seekbar.getMax());

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress_value;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value = progress;
                seekbar_counter.setText(progress+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekbar_counter.setText(progress_value+"");
            }
        });
    }





}
