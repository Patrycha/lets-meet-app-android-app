package rest.letsmeet.org.letsmeetapp.json;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.joda.time.LocalTime;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TimeDeserializer implements JsonDeserializer<LocalTime>{

    private static final String TIME_FORMAT = "HH:mm:ss";

    @Override
    public LocalTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        try {
            String s = json.getAsString();
            SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT);
            long ms = sdf.parse(s).getTime();
           // Time t = new Time(ms);
            LocalTime t = new LocalTime(ms);
            return t;
        } catch (ParseException e) {
            throw new JsonParseException("Unparseable time: \"" + json.getAsString()
                    + "\". Supported formats: " + TIME_FORMAT);
        }
    }
}
