package rest.letsmeet.org.letsmeetapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Or;

import org.greenrobot.eventbus.EventBus;

import rest.letsmeet.org.letsmeetapp.adapter.GroupFragmentPagerAdapter;
import rest.letsmeet.org.letsmeetapp.busevent.NewGroupEvent;
import rest.letsmeet.org.letsmeetapp.model.Category;
import rest.letsmeet.org.letsmeetapp.model.Group;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;

public class GroupsTabsActivity extends BaseActivity implements View.OnClickListener {

    private long userID;
    private long categoryID;
    private Context context;

    FloatingActionButton button;
    Dialog dialog;
    Button cancel_btn;
    Button confirm_btn;

    @NotEmpty(message = "Pole obowiązkowe!")
    @Or
    @Length(min = 5, max = 50, message = "Niepoprawna długość. Od 5 do 50 znaków.")
    EditText name;

    @Length(max = 255, message = "Niepoprawna długość. Maksymalnie 255 znaków.")
    EditText description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = getApplicationContext();
        Bundle extras = getIntent().getExtras();

        if(extras != null){
            categoryID = extras.getLong("categoryID");
            userID = extras.getLong(USER_ID);
        }else{
           // Toast.makeText(context, "Bundle is null", Toast.LENGTH_SHORT).show();
        }
        //Toast.makeText(context, "groups:userID: " + userID, Toast.LENGTH_SHORT).show();

        //setContentView(R.layout.groups_tabs_layout);

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.groups_tabs_layout, contentFrameLayout);

        /* set list */
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        GroupFragmentPagerAdapter adapter = new GroupFragmentPagerAdapter(this, getSupportFragmentManager(), extras);

        viewPager.setAdapter(adapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

        /*button*/
        button = (FloatingActionButton) findViewById(R.id.add_group_button);
        button.setOnClickListener(this);

        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Category> call = service.getCategory(categoryID);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<Category>() {
                @Override
                public void onResponse(Call<Category> call, Response<Category> response) {

                    if (response.code() == 200) {
                        if (response.body() != null) {
                           toolbar.setTitle(response.body().getName());
                        }
                    }
                }

                @Override
                public void onFailure(Call<Category> call, Throwable t) {
                   // Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Log.e("Error in getCategory", e.getMessage());
        }
    }

    @Override
    public void onClick(View v) {
        dialog = new ValidatorDialog(v.getContext(), 0);//?
        dialog.setContentView(R.layout.group_add_dialog);

        cancel_btn = (Button) dialog.findViewById(R.id.newgroup_cancel_btn);
        cancel_btn.setText(R.string.btn_text_cancel);
        confirm_btn = (Button) dialog.findViewById(R.id.newgroup_create_btn);
        confirm_btn.setText(R.string.btn_text_create);
        name = (EditText) dialog.findViewById(R.id.edit_newgroup_name);
        description = (EditText) dialog.findViewById(R.id.edit_newgroup_descr);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        confirm_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ValidatorDialog)dialog).onClick(v);

                final Group group;
                if(((ValidatorDialog)dialog).validated) {
                 //   Toast.makeText(context, "VALIDATED", Toast.LENGTH_SHORT).show();

                    group = new Group(name.getText().toString(), description.getText().toString());
                    final Dialog dialog_confirm = new Dialog(v.getContext());
                    dialog_confirm.setContentView(R.layout.confirm_dialog);
                    cancel_btn = (Button) dialog_confirm.findViewById(R.id.cancel_btn);
                    cancel_btn.setText(R.string.btn_text_cancel);
                    confirm_btn = (Button) dialog_confirm.findViewById(R.id.confirm_btn);
                    confirm_btn.setText(R.string.btn_text_create);
                    TextView description_tv = (TextView) dialog_confirm.findViewById(R.id.descr_tv);
                    TextView title_tv = (TextView) dialog_confirm.findViewById(R.id.title_tv);
                    title_tv.setText("Czy na pewno chcesz utworzyć grupę?");
                    description_tv.setText("Tworząc grupę stajesz się jej administratorem. Każdy będzie mógł do niej dołączyć.");

                    cancel_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog_confirm.dismiss();
                            dialog.dismiss();
                        }
                    });
                    confirm_btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            createGroup(group, categoryID, userID);
                            dialog_confirm.dismiss();
                            dialog.dismiss();
                        }
                    });

                     dialog_confirm.show();

                }

            }
        });

        dialog.show();
    }

    private void createGroup(Group group, long categoryId, long userId) {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Void> call = service.createGroup(categoryId, group, userId);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<Void>() {
                @Override
                public void onResponse(Call<Void> call, Response<Void> response) {

                    if (response.code() == 201) {//HtttpStatus.created
                        EventBus.getDefault().post(new NewGroupEvent("changed"));
                    }
                    else if(response.code() == 409){//HtttpStatus.conflict
                        Toast.makeText(context, "Grupa o takiej nazwie już istnieje", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Void> call, Throwable t) {
                    //Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Log.e("Error in createGroup", e.getMessage());
        }
    }

}
