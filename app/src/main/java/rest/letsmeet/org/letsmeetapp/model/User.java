package rest.letsmeet.org.letsmeetapp.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class User implements Serializable{

    @Expose
    private long id;
    @Expose
    private String login;
    //@Expose
    private String password;
   // @Expose
    private String email;
    @Expose
    private String name;
    @Expose
    private String surname;
  //  @Expose
    private Date birthDate;
    @Expose
    private boolean isAdmin;
    @Expose
    private String description;
    @Expose
    private char sex;
  //  @Expose
    private Date joiningDate;
    @Expose
    private String imagePath;
    @Expose
    private int absencesCount;
    @Expose
    private int cancellingsCount; //after permissive time
    @Expose
    private int gradesCount;
    @Expose
    private double average;

	/*fk*/

   // private City city;
   @Expose
    private List<Group> administratesGroups;
    @Expose
    private List<Group> joinedGroups;

   //constructors

    public User() {}

    public User(long id) {
        this.id = id;
    }

    //getters

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLogin() {
        return login;
    }

    public String getImagePath() {
        return imagePath;
    }

    public long getId() {
        return id;
    }

    //setters

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public void setJoiningDate(Date joiningDate) {
        this.joiningDate = joiningDate;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
