package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.busevent.ChangeParticipationStatusEvent;
import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.UsersRVAdapter;
import rest.letsmeet.org.letsmeetapp.adapter.UsersRVAdapter_EventAdmin;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.EVENT_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;

public class ParticipantFragment extends android.support.v4.app.Fragment {
    private long userID;
    private long eventID;
    String status;

    private RecyclerView rv ;
    private LinearLayoutManager llm;
    private UsersRVAdapter rva;
    private UsersRVAdapter_EventAdmin rva_admin;

    public ParticipantFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            eventID = extras.getLong(EVENT_ID);
            userID = extras.getLong(USER_ID);
        }else{
            eventID = -1;
            userID = -1;
        }
        status = "";
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.users_fragment, container, false);
        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv_users);
        rv.setLayoutManager(llm);

        if(eventID != -1){
            getParticipants(eventID, status);
        }
        else{
            Toast.makeText(getActivity(), "nie przekazano id wydarzenia", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    private void getParticipants(final long eventID, String status) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            //Call<List<User>> call = service.getParticipants1( status, eventID);
            Call<List<User>> call = service.getParticipants(eventID, status);
            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                    if(response.code() == 200){
                        if(response.body() == null){
                            Toast.makeText(getContext(), "Body is null", Toast.LENGTH_SHORT).show();
                        }else{
                            showUsers(eventID, userID, response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error in getEventList",e.getMessage());
        }
    }


    private void showUsers(long eventId, long userId, final List<User> list) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            final Call<Boolean> call = service.isEventAdmin(eventId, userId);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());
            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if(response.code() == 200){
                        if(response.body() != null){
                            boolean isEventAdmin = response.body();
                            //Toast.makeText(getContext(), "isAdmin: "+isEventAdmin, Toast.LENGTH_SHORT).show();

                            if(isEventAdmin){
                                rva_admin = new UsersRVAdapter_EventAdmin(getContext(), list, eventID, userID, status);
                                rv.setAdapter(rva_admin);
                            }else{
                                rva = new UsersRVAdapter(getContext(), list);
                                rv.setAdapter(rva);
                            }
                        }
                    }
                }
                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    Log.d("showUsers", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in isEventAdmin",e.getMessage());
        }
    }

    @Override
    public void onDetach() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Subscribe
    public void onEvent(ChangeParticipationStatusEvent event){
        Log.d("EVENT", "ParticipationStatusChange");
        getParticipants(eventID, status);
    }


}
