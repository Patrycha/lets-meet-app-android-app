package rest.letsmeet.org.letsmeetapp.navigation_drawer;


import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.internal.ScrimInsetsFrameLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_IMAGE_PATH;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_LOGIN;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_NAME;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_SURNAME;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.readFromPreferences;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.saveToPreferences;

public class NavigationDrawerFragment extends Fragment implements NavigationDrawerCallbacks {
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";

    private NavigationDrawerCallbacks mCallbacks;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private View containerView;
    private RecyclerView mDrawerList;
    private ImageView mAvatar;
    private TextView mFullName;
    private TextView mLogin;

    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private int mCurrentSelectedPosition;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "false"));

        if(savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer_, container, false);
        mDrawerList = (RecyclerView) view.findViewById(R.id.drawerList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mDrawerList.setLayoutManager(layoutManager);
        mDrawerList.setHasFixedSize(true);

        mAvatar = (ImageView) view.findViewById(R.id.imgAvatar);
        mLogin = (TextView)view.findViewById(R.id.txtUserLogin);
        mFullName = (TextView)view.findViewById(R.id.txtUsername);
        setDrawerHeader(mAvatar, mLogin, mFullName);
        
        final List<NavigationItem> navigationItems = getMenu();
        NavigationDrawerAdapter adapter = new NavigationDrawerAdapter(navigationItems);
        adapter.setNavigationDrawerCallbacks(this);
        mDrawerList.setAdapter(adapter);
        selectItem(mCurrentSelectedPosition);
    
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

    public void setActionBarDrawerToggle(ActionBarDrawerToggle actionBarDrawerToggle) {
        mActionBarDrawerToggle = actionBarDrawerToggle;
    }

    private void setDrawerHeader(ImageView mAvatar, final TextView mLogin, final TextView mFullName) {
        String name = readFromPreferences(getContext(), USER_NAME, "");
        String surname = readFromPreferences(getContext(), USER_SURNAME, "");
        String login = readFromPreferences(getContext(), USER_LOGIN, "");
        long user_id = Long.valueOf(readFromPreferences(getContext(), USER_ID, "-1"));
        String imagePath = readFromPreferences(getContext(), USER_IMAGE_PATH, "");

        if(!name.equals("") && !surname.equals("") && !login.equals("") ){
            mFullName.setText(name + " " + surname);
            mLogin.setText(login);
            //TODO: use Picasso library
           // Picasso.with(getContext()).load("http://i.imgur.com/DvpvklR.png").error(R.drawable.default_avatar).into(mAvatar);

            //Toast.makeText(getContext(), "data from SharedPreferences", Toast.LENGTH_SHORT).show();
            //Toast.makeText(getContext(), "shared:userID: " + user_id, Toast.LENGTH_SHORT).show();

        }
        else{
            try{
                APIService service = ApiClient.getRetrofit().create(APIService.class);
                Call<User> call = service.getUser(user_id);

                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        User user = response.body();

                        if(user != null){
                            saveToPreferences(getContext(), USER_NAME, user.getName());
                            saveToPreferences(getContext(), USER_SURNAME, user.getSurname());
                            saveToPreferences(getContext(), USER_LOGIN, user.getLogin());
                            saveToPreferences(getContext(), USER_IMAGE_PATH, user.getImagePath());
                            Toast.makeText(getContext(), "data from server", Toast.LENGTH_SHORT).show();
                            mFullName.setText(user.getName() + " " + user.getSurname());
                            mLogin.setText(user.getLogin());
                            //TODO: use Picasso library
                            // Picasso.with(getContext()).load("http://i.imgur.com/DvpvklR.png").error(R.drawable.default_avatar).into(mAvatar);
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (Exception e){
                Log.e("Error in fetching cat.",e.getMessage());
            }
        }

    }

    public void setUp(final Activity activity, int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        containerView = activity.findViewById(fragmentId);
        if(containerView.getParent() instanceof ScrimInsetsFrameLayout){
            containerView = (View) containerView.getParent();
        }
        mDrawerLayout = drawerLayout;

        mActionBarDrawerToggle = new ActionBarDrawerToggle(activity, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

               // if(!mUserLearnedDrawer) {
               //     mUserLearnedDrawer = true;
               //     saveToPreferences(activity, KEY_USER_LEARNED_DRAWER, mUserLearnedDrawer+"");
              //  }
                if (!isAdded()) return ;
                activity.invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return ;
                if(mUserLearnedDrawer){
                    mUserLearnedDrawer = true;
                    saveToPreferences(activity, KEY_USER_LEARNED_DRAWER, "true");

                }
                activity.invalidateOptionsMenu();
            }
        };

        if (!mUserLearnedDrawer && !mFromSavedInstanceState){
            mDrawerLayout.openDrawer(containerView);
        }

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(containerView);
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(containerView);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public List<NavigationItem> getMenu() {
        List<NavigationItem> items = new ArrayList<NavigationItem>();

        String[] labels = getContext().getResources().getStringArray(R.array.drawer_items);

        for ( int i = 0 ; i < labels.length ; i++)
            items.add(new NavigationItem(labels[i], null));

        return items;
    }
    /**
     * Changes the icon of the drawer to back
     */
    public void showBackButton() {
        if (getActivity() instanceof ActionBarActivity) {
            ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * Changes the icon of the drawer to menu
     */
    public void showDrawerButton() {
        if (getActivity() instanceof ActionBarActivity) {
            ((ActionBarActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
        mActionBarDrawerToggle.syncState();
    }
    void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(containerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
        ((NavigationDrawerAdapter) mDrawerList.getAdapter()).selectPosition(position);
    }
    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(containerView);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }
    @Override
    public void onNavigationDrawerItemSelected(int position) {
       selectItem(position);
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
    }
}
