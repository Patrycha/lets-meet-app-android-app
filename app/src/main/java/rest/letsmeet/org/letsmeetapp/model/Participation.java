package rest.letsmeet.org.letsmeetapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class Participation {

    private long id;

    private Date date;

    private ParticipationStatus status;

    @JsonProperty("event")
    private Event event;

    private boolean thisUserMadeRecentChange;

    public Participation(){}

    public long getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public ParticipationStatus getStatus() {
        return status;
    }

    public Event getEvent() {
        return event;
    }

    public boolean haveThisUserMadeRecentChange() {
        return thisUserMadeRecentChange;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setStatus(ParticipationStatus status) {
        this.status = status;
    }

    public void setThisUserMadeRecentChange(boolean thisUserMadeRecentChange) {
        this.thisUserMadeRecentChange = thisUserMadeRecentChange;
    }
}
