package rest.letsmeet.org.letsmeetapp.tab;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.NotificationRVAdapter;
import rest.letsmeet.org.letsmeetapp.model.Notification;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.NOTIFICATION_CREATED;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.readFromPreferences;

public class NotificationsFragment extends android.support.v4.app.Fragment {
    private long userID;

    private RecyclerView rv ;
    private LinearLayoutManager llm;
    private NotificationRVAdapter rva;

    public NotificationsFragment(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        userID = Long.parseLong(readFromPreferences(getContext(), USER_ID, "-1"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notification_fragment, container, false);
        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv);
        rv.setLayoutManager(llm);

        if(userID != -1){
            getNotifications(userID, NOTIFICATION_CREATED);
            showNotifications(userID, null);
        }
        else{
            //Toast.makeText(getActivity(), "nie przekazano id uzytkownika", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    private void getNotifications(final long userId, String status) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<Notification>> call = service.getNotifications(userId, status);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<Notification>>() {
                @Override
                public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {

                    if(response.isSuccessful()){
                           // showNotifications(userId, response.body());
                            rva.updateDataSet(response.body());
                            rva.notifyDataSetChanged();

                    }
                }

                @Override
                public void onFailure(Call<List<Notification>> call, Throwable t) {
                   Log.d("getNotifications", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getNotif...",e.getMessage());
        }
    }

    private void showNotifications(long userId, List<Notification> list) {
        rva = new NotificationRVAdapter(getContext(), userId, list);
        rv.setAdapter(rva);
    }
}
