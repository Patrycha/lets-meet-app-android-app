package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.tab.AllGroupsFragment;
import rest.letsmeet.org.letsmeetapp.tab.MyGroupsFragment;

public class GroupFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private Bundle bundle;

    public GroupFragmentPagerAdapter(Context context, FragmentManager fm, Bundle bundle) {
        super(fm);
        this.mContext = context;
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;

        if (position == 0)
            fragment = new AllGroupsFragment();
        else
            fragment = new MyGroupsFragment();

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String[] titles = mContext.getResources().getStringArray(R.array.groups_tabs);

        switch (position) {
            case 0:
                return titles[0];
            case 1:
                return titles[1];
            default:
                return null;
        }
    }
}
