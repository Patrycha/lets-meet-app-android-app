package rest.letsmeet.org.letsmeetapp.json;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdScalarSerializer;

import org.joda.time.LocalTime;
import org.joda.time.format.ISODateTimeFormat;

import java.io.IOException;

public class LocalTimeSerializer1 extends StdScalarSerializer<LocalTime> {

    protected LocalTimeSerializer1(Class<LocalTime> t) {
        super(t);
    }

    protected LocalTimeSerializer1() {
        super(LocalTime.class);
    }

    @Override
    public void serialize(LocalTime value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
        String timeAsString = ISODateTimeFormat.basicTime().print(value);
        jgen.writeString(timeAsString);
    }
}
