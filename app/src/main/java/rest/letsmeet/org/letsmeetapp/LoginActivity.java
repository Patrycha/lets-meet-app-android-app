package rest.letsmeet.org.letsmeetapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import butterknife.BindView;
import butterknife.ButterKnife;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_IMAGE_PATH;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_LOGIN;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_NAME;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_SURNAME;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.saveToPreferences;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;

    @BindView(R.id.input_login) EditText _loginText;
    @BindView(R.id.input_password) EditText _passwordText;
    @BindView(R.id.btn_login) Button _loginButton;
    @BindView(R.id.link_signup) TextView _signupLink;

    private ProgressDialog progressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        _loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                login();
            }
        });

        _signupLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Start the Signup activity
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivityForResult(intent, REQUEST_SIGNUP);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });
    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            return;
        }

        _loginButton.setEnabled(false);

/*      progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.Theme_AppCompat_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Trwa logowanie...");
        progressDialog.show();
*/
        String login = _loginText.getText().toString();
        String password = _passwordText.getText().toString();

        LoginUserTask task = new LoginUserTask(login, password, this);

        task.execute((Void)null);


/*
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        // On complete call either onLoginSuccess or onLoginFailed
                        onLoginSuccess();
                        // onLoginFailed();
                        progressDialog.dismiss();
                    }
                }, 3000);
                */
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SIGNUP) {
            if (resultCode == RESULT_OK) {

                // TODO: Implement successful signup logic here
                // By default we just finish the Activity and log them in automatically
               // this.finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        // Disable going back to the MainActivity
        moveTaskToBack(true);
    }

    public void onLoginSuccess() {
        Toast.makeText(getApplicationContext(), "Zalogowano pomyślnie", Toast.LENGTH_SHORT).show();
        _loginButton.setEnabled(true);
    }

    public void onLoginFailed() {
        Toast.makeText(getApplicationContext(), "Logowanie nie powiodło się", Toast.LENGTH_SHORT).show();
        _loginButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String login = _loginText.getText().toString();
        String password = _passwordText.getText().toString();

        if (login.isEmpty() || login.length() < 5 || login.length() > 30) {
            _loginText.setError("od 5 do 30 znaków");
            valid = false;
        } else {
            _loginText.setError(null);
        }

        if (password.isEmpty() || password.length() < 5 || password.length() > 30) {
            _passwordText.setError("od 5 do 30 znaków");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    public class LoginUserTask extends AsyncTask<Void, Void, Boolean>{
        private Response<User> response;
        private String login;
        private String password;
        private Context context;

        public LoginUserTask(String l, String p, Context context){
            login = l;
            password = p;
            this.context = context;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            APIService service = ApiClient.getRetrofit().create(APIService.class);

            String base = login + ":" + password;
            String authHeader = "Basic " + Base64.encodeToString(base.getBytes(StandardCharsets.UTF_8), Base64.NO_WRAP);
            Call<User> call = service.getUser(authHeader);

            try {
                response = call.execute();

                //progressDialog.dismiss();

                if(response.isSuccessful()) {

                    if (response.body() != null)
                        return true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (response.isSuccessful()){
                onLoginSuccess();
                Intent intent = new Intent(context, CategoriesActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong(USER_ID, response.body().getId());
                intent.putExtras(bundle);
                saveToPreferences(context, USER_ID, String.valueOf(response.body().getId()));//2nd option
                saveToPreferences(context, USER_NAME, String.valueOf(response.body().getName()));//2nd option
                saveToPreferences(context, USER_SURNAME, String.valueOf(response.body().getSurname()));//2nd option
                saveToPreferences(context, USER_LOGIN, String.valueOf(response.body().getLogin()));//2nd option
                saveToPreferences(context, USER_IMAGE_PATH, String.valueOf(response.body().getImagePath()));//2nd option

                finish();
                startActivity(intent);
            }else{
                onLoginFailed();
            }
        }


    }

}
