package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.UsersRVAdapter;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_ACCEPTED;


public class EventDetailsFragment_Users extends android.support.v4.app.Fragment {
    private long userID;
    private long eventID;

    RecyclerView rv ;
    LinearLayoutManager llm;
    UsersRVAdapter rva;

    public EventDetailsFragment_Users(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            eventID = extras.getLong("eventID");
            userID = extras.getLong("userID");
        }else{
            eventID = -1;
            userID = -1;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.users_fragment, container, false);//TODO : może trzeb zmienić, bo ten sam do członków grupy

        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv_users);
        rv.setLayoutManager(llm);

        if(eventID != -1){
            getUserList(eventID);
        }
        else{
            //Toast.makeText(getActivity(), "nie przekazano id eventu", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    private void getUserList(long eventId) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<User>> call = service.getParticipants(eventId, PARTICIPATION_ACCEPTED);//todo -> czy na pewno taki

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                    if(response.code() == 200){
                        if(response.body() == null){
                           // Toast.makeText(getContext(), "Body is null", Toast.LENGTH_SHORT).show();
                        }else{
                            showUsers(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                   // Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error in getUserList",e.getMessage());
        }
    }

    private void showUsers(List<User> users) {
        rva = new UsersRVAdapter(getContext(), users);
        rv.setAdapter(rva);
    }
}
