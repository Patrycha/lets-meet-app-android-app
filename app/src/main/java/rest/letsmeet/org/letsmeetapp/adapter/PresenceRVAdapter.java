package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.Event;

public class PresenceRVAdapter extends RecyclerView.Adapter<PresenceRVAdapter.MyViewHolder>{
    private long userID;

    private List<Event> list;
    private Context context;

    public PresenceRVAdapter(Context context, List<Event> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.presence_event_layout, parent, false);
        MyViewHolder gvh = new MyViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText("Name: " + list.get(position).getName());

       // Participation p = new Participation();//usera 1
       // List<Event> events = p.get();
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title;

        public MyViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
        }
    }
}
