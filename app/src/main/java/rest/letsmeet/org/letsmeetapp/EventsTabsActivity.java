package rest.letsmeet.org.letsmeetapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Or;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.LocalTime;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import rest.letsmeet.org.letsmeetapp.adapter.EventFragmentPagerAdapter;
import rest.letsmeet.org.letsmeetapp.adapter.GroupDetailsFragmentPagerAdapter;
import rest.letsmeet.org.letsmeetapp.adapter.UserFragmentPagerAdapter;
import rest.letsmeet.org.letsmeetapp.busevent.NewEventEvent;
import rest.letsmeet.org.letsmeetapp.constant.Constants;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.model.Group;
import rest.letsmeet.org.letsmeetapp.model.VisibilityStatus;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventsTabsActivity extends BaseActivity implements Constants {

    private long userID;
    private long groupID;
    private long groupAdminID;
    private Context context;

    private BottomNavigationView bottomNavigation;

    FloatingActionButton fab_plus, fab_walk, fab_more;
    TabLayout tabLayout;
    Animation FabOpen, FabClose, FabRClockwise , FabRanticlockwise;
    boolean isOpen = false;

    Dialog dialog;
    Button cancel_btn, confirm_btn;
    TextView title, description;
    TextView date_bigin, date_end;
    ImageButton calendar_btn_start;
    ImageButton calendar_btn_end;

    SeekBar seekbar;
    TextView seekbar_limit;

    @NotEmpty(message = "Pole obowiązkowe")
    @Or
    @Length(min = 10, max = 50, message = "Niepoprawna długość")
    EditText name_tv;

    @Length(max = 255, message = "Niepoprawna długość")
    EditText descr_tv;

    @Length(max = 100, message = "Niepoprawna długość")
    EditText place_tv;

    @Max(value = 100, message = "Maksymalnie 100 użytkowników")
    @Or
    @Min(value = 1, message = "Minimum 1 użytkownik")
    EditText seekbar_counter;

    CheckBox visibility_status_chb;

    Date event_start_date, event_end_date;
    LocalTime event_start_time, event_end_time;
    private String group_descr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        final Bundle extras = getIntent().getExtras();

        if(extras != null){
            groupID = extras.getLong("groupID");
            userID = extras.getLong("userID");
            groupAdminID = extras.getLong("groupAdminID");
        }else{
            Log.e("EventsTabsA", "Bundle is null");
        }
       // setContentView(R.layout.events_tab_layout);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.events_tab_layout, contentFrameLayout);
       // ButterKnife.bind(this);
        /* set list */
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager_e);

        EventFragmentPagerAdapter adapter = new EventFragmentPagerAdapter(this, getSupportFragmentManager(), extras);

        viewPager.setAdapter(adapter);

        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);

         /* set list */
        final ViewPager viewPager2 = (ViewPager) findViewById(R.id.viewpager_u);
        UserFragmentPagerAdapter adapter2 = new UserFragmentPagerAdapter(this, getSupportFragmentManager());
        viewPager2.setAdapter(adapter2);

        final ViewPager viewPager3 = (ViewPager) findViewById(R.id.viewpager_details);

        /*buttons*/
        setFabs();

        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Group> call = service.getGroup(groupID);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<Group>() {
                @Override
                public void onResponse(Call<Group> call, Response<Group> response) {

                    if (response.code() == 200) {
                        if (response.body() != null) {
                            toolbar.setTitle(response.body().getName());
                            group_descr =  response.body().getDescription();

                            GroupDetailsFragmentPagerAdapter adapter3 = new GroupDetailsFragmentPagerAdapter(context, getSupportFragmentManager(), group_descr, groupAdminID);
                            viewPager3.setAdapter(adapter3);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Group> call, Throwable t) {
                   // Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Log.e("Error in getGroup", e.getMessage());
        }

        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);//TODO: redundant

        final TabLayout finalTabLayout = tabLayout;
        bottomNavigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch(item.getItemId()){
                    case R.id.bottom_navigation_details:
                        finalTabLayout.setVisibility(View.INVISIBLE);
                        tabLayout.setVisibility(View.INVISIBLE);
                        viewPager.setVisibility(View.INVISIBLE);
                        viewPager2.setVisibility(View.INVISIBLE);
                        viewPager3.setVisibility(View.VISIBLE);
                        break;
                    case R.id.bottom_navigation_users:
                        finalTabLayout.setVisibility(View.INVISIBLE);
                        tabLayout.setVisibility(View.INVISIBLE);
                        viewPager.setVisibility(View.INVISIBLE);
                        viewPager2.setVisibility(View.VISIBLE);
                        viewPager3.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.bottom_navigation_events:
                        finalTabLayout.setVisibility(View.VISIBLE);
                        tabLayout.setVisibility(View.VISIBLE);
                        viewPager.setVisibility(View.VISIBLE);
                        viewPager2.setVisibility(View.INVISIBLE);
                        viewPager3.setVisibility(View.INVISIBLE);
                        break;
                    default:
                        break;
                }
                return true;
            }
        }
        );
    }

    private void setFabs() {
        fab_more = (FloatingActionButton) findViewById(R.id.fab_more);
        fab_plus = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_walk = (FloatingActionButton) findViewById(R.id.fab_walk);
        FabOpen = AnimationUtils.loadAnimation(context, R.anim.fab_open);
        FabClose = AnimationUtils.loadAnimation(context, R.anim.fab_close);
        FabRClockwise = AnimationUtils.loadAnimation(context, R.anim.rotate_clockwise);
        FabRanticlockwise = AnimationUtils.loadAnimation(context, R.anim.rotate_anticlockwise);

        fab_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isOpen){
                    fab_walk.startAnimation(FabClose);
                    fab_plus.startAnimation(FabClose);
                    fab_more.startAnimation(FabRanticlockwise);
                    fab_walk.setClickable(false);
                    fab_plus.setClickable(false);
                    isOpen = false;
                }
                else{
                    fab_walk.startAnimation(FabOpen);
                    fab_plus.startAnimation(FabOpen);
                    fab_more.startAnimation(FabRClockwise);
                    fab_walk.setClickable(true);
                    fab_plus.setClickable(true);
                    isOpen = true;
                }
            }
        });

        fab_walk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new Dialog(v.getContext());
                dialog.setContentView(R.layout.confirm_dialog);
                cancel_btn = (Button) dialog.findViewById(R.id.cancel_btn);
                confirm_btn = (Button) dialog.findViewById(R.id.confirm_btn);
                description = (TextView) dialog.findViewById(R.id.descr_tv);
                title = (TextView) dialog.findViewById(R.id.title_tv);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                confirm_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        joinOrLeaveGroup(userID, groupID);
                        dialog.dismiss();
                    }
                });

                try{
                    APIService service = ApiClient.getRetrofit().create(APIService.class);
                    Call<Boolean> call = service.isSubscribing(userID, groupID);

                    call.enqueue(new Callback<Boolean>() {
                        @Override
                        public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                            if(response.code() == 200){
                                if(groupAdminID == userID){
                                    title.setText("Nie możesz opuścić grupy");
                                    description.setText("Jesteś administratorem grupy.");
                                    confirm_btn.setText("ROZUMIEM");
                                    confirm_btn.setClickable(true);
                                    confirm_btn.setVisibility(View.VISIBLE);
                                    cancel_btn.setVisibility(View.INVISIBLE);
                                    cancel_btn.setClickable(false);
                                    confirm_btn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                }else if (response.body()){
                                    title.setText("Czy na pewno chcesz opuścić grupę?");
                                    description.setText("Nie będziesz dostawał dotyczących jej powiadomień.");
                                    confirm_btn.setText(context.getString(R.string.btn_text_leave));
                                    confirm_btn.setClickable(true);
                                    confirm_btn.setVisibility(View.VISIBLE);
                                    cancel_btn.setVisibility(View.VISIBLE);
                                    cancel_btn.setClickable(true);
                                }else {
                                    title.setText("Czy na pewno chcesz dołączyć do grupy?");
                                    description.setText("Jeśli to zrobisz będziesz dostawał powiadomienia dotyczące grupy.");
                                    confirm_btn.setText(context.getString(R.string.btn_text_join));
                                    confirm_btn.setClickable(true);
                                    confirm_btn.setVisibility(View.VISIBLE);
                                    cancel_btn.setVisibility(View.VISIBLE);
                                    cancel_btn.setClickable(true);
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<Boolean> call, Throwable t) {
                          //  Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                        }
                    });
                }catch (Exception e){
                    Log.e("Error in fetching data",e.getMessage());
                }

                dialog.show();
            }
        });

        fab_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new ValidatorDialog(v.getContext(), android.R.style.Theme_Holo_Light_NoActionBar_Fullscreen);
                dialog.setContentView(R.layout.full_screen_dialog);
                date_bigin = (TextView) dialog.findViewById(R.id.newevent_date_begin);
                date_end = (TextView) dialog.findViewById(R.id.newevent_date_end);
                calendar_btn_start = (ImageButton) dialog.findViewById(R.id.newevent_calendar_btn);
                calendar_btn_end = (ImageButton) dialog.findViewById(R.id.newevent_calendar_btn_1);
                setSeekbar(dialog);
                //settings
                final TimePickerDialog beginTimePickerDialog = new TimePickerDialog(EventsTabsActivity.this, R.style.DateTimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        event_start_time = new LocalTime(Time.valueOf(hourOfDay+":"+minute+":"+0));;//LocalTime.fromMillisOfDay(Time.valueOf(hourOfDay+":"+minute+":"+0).getTime());
                        CharSequence temp = date_bigin.getText();
                        date_bigin.setText(temp + " " + (""+event_start_time).substring(0,5));
                    }
                }, 12, 0, true);

                final TimePickerDialog endTimePickerDialog = new TimePickerDialog(EventsTabsActivity.this, R.style.DateTimePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        event_end_time = new LocalTime(Time.valueOf(hourOfDay+":"+minute+":"+0));//LocalTime.fromMillisOfDay(Time.valueOf(hourOfDay+":"+minute+":"+0).getTime());
                        CharSequence temp = date_end.getText();
                        date_end.setText(temp + " " + (""+event_end_time).substring(0,5));
                    }
                }, 12, 0, true);

                final Calendar calendar = Calendar.getInstance();
                int y = calendar.get(Calendar.YEAR);
                int m = calendar.get(Calendar.MONTH);
                int d = calendar.get(Calendar.DAY_OF_MONTH);

                final DatePickerDialog endDatePickerDialog = new DatePickerDialog(EventsTabsActivity.this, R.style.DateTimePickerTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        endTimePickerDialog.show();

                        event_end_date = new Date(year-1900, month, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DMY);
                        date_end.setText(simpleDateFormat.format(event_end_date));

                    }
                }, y, m, d);

                 final DatePickerDialog beginDatePickerDialog = new DatePickerDialog(EventsTabsActivity.this, R.style.DateTimePickerTheme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        beginTimePickerDialog.show();

                        event_start_date = new Date(year-1900, month, dayOfMonth);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DMY);
                        date_bigin.setText(simpleDateFormat.format(event_start_date));

                        //
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(event_start_date);
                        cal.set(Calendar.HOUR_OF_DAY, 0);
                        cal.set(Calendar.MINUTE, 0);
                        cal.set(Calendar.SECOND, 0);
                        cal.set(Calendar.MILLISECOND, 0);

                        endDatePickerDialog.getDatePicker().setMinDate(cal.getTime().getTime());
                    }
                }, y, m, d);

                beginDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                //endDatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

                calendar_btn_start.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        beginDatePickerDialog.show();
                    }
                }
                );
                calendar_btn_end.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        endDatePickerDialog.show();
                    }
                });
                //fields
                name_tv = (EditText) dialog.findViewById(R.id.edit_newevent_name);
                descr_tv = (EditText) dialog.findViewById(R.id.edit_newevent_descr);
                place_tv = (EditText) dialog.findViewById(R.id.edit_newevent_place);
                visibility_status_chb = (CheckBox) dialog.findViewById(R.id.newevent_visibility_checkbox);
                //buttons
                confirm_btn = (Button) dialog.findViewById(R.id.newevent_create_btn);

                cancel_btn = (Button)dialog.findViewById(R.id.newevent_cancel_btn);

                cancel_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                confirm_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((ValidatorDialog)dialog).onClick(v);

                        String name = name_tv.getText().toString();
                        String descr = descr_tv.getText().toString();
                        String place = place_tv.getText().toString();
                        boolean is_private = visibility_status_chb.isChecked();
                        int limit = Integer.parseInt(seekbar_counter.getText().toString());

                        //try to save new event
                        if(((ValidatorDialog)dialog).validated){
                            //TODO: date and time validation
                            saveNewEvent(name, descr, place, is_private, limit, event_start_time, event_start_date, event_end_date, event_start_time, event_end_time, userID, groupID);
                            dialog.dismiss();
                        }
                    }
                });

                dialog.show();

            }
        });
    }

    private void saveNewEvent(String name, String descr, String place, boolean is_private, int users_limit, LocalTime event_start_time, Date event_start_date, Date event_end_date, LocalTime time_start, LocalTime time_end, long userID, long groupID) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);

            Event event = new Event(0,name, descr);
            event.setPlace(place);
            event.setParticipantsLimit(users_limit);
            event.setVisibility(new VisibilityStatus(0, is_private ? VISIBILITY_PRIVATE : VISIBILITY_PUBLIC));
            event.setBeginningDate(event_start_date);
            event.setEndingDate(event_end_date);
            event.setBeginningTime(time_start);//TODO good
            event.setEndingTime(time_end);//TODO good

            Call<Boolean> call = service.addNewEvent(userID, groupID, event);

            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if(response.code() == 200){
                        EventBus.getDefault().post(new NewEventEvent("changed"));
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    //Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error newEvent",e.getMessage());
        }
    }

    private void joinOrLeaveGroup(final long userId, final long groupId) {
        //1. check if user is part of the group
        //2. subscribe ot unsubscribe
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Boolean> call = service.isSubscribing(userId, groupId);

            //  Log.d(String.valueOf(Log.INFO), service.getGroups().request().url().toString());

            call.enqueue(new Callback<Boolean>() {
                @Override
                public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                    if(response.code() == 200){

                        if(!response.body()){
                            setSubscribtion(userId, groupId);
                        }else{
                            unsetSubscribtion(userId, groupId);
                        }
                    }
                }

                @Override
                public void onFailure(Call<Boolean> call, Throwable t) {
                    //Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error joinOrLeave",e.getMessage());
        }
    }

    private void setSubscribtion(long userId, long groupId) {
        APIService service = ApiClient.getRetrofit().create(APIService.class);

        service.setSubsciption(userId, groupId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if(response.code() == 201){
                    //Toast.makeText(context, "Zapisano", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
               // Toast.makeText(context, t.toString(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void unsetSubscribtion(long userId, long groupId) {
        APIService service = ApiClient.getRetrofit().create(APIService.class);

        service.unsetSubscribtion(userId, new Long(groupId)).enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if(response.code() == 200){
                    //Toast.makeText(context, "Wypisano", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                //Toast.makeText(context, t.toString(),Toast.LENGTH_LONG).show();
            }
        });
    }

    public void setSeekbar(Dialog dialog){
        seekbar = (SeekBar) dialog.findViewById(R.id.newevent_seekbar);
        seekbar_counter = (EditText) dialog.findViewById(R.id.newevent_seekBar_count);
        seekbar_limit = (TextView) dialog.findViewById(R.id.newevent_seekbar_count_max);

        seekbar_counter.setText(seekbar.getProgress()+"");
        seekbar_limit.setText(" / " + seekbar.getMax());

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            int progress_value;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress_value = progress;
                seekbar_counter.setText(progress+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekbar_counter.setText(progress_value+"");
            }
        });
    }
}
