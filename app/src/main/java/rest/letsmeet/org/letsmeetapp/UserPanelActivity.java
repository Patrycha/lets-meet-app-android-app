package rest.letsmeet.org.letsmeetapp;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import rest.letsmeet.org.letsmeetapp.adapter.Groups_FPAdapter;
import rest.letsmeet.org.letsmeetapp.adapter.Notes_FPAdapter;
import rest.letsmeet.org.letsmeetapp.tab.NotificationsFragment;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.readFromPreferences;

public class UserPanelActivity extends BaseActivity {

    private long userID;
    private Context context;
    private Bundle extras;

    private TabLayout tabLayout;
    private TabLayout tabLayout_group;

    private FrameLayout frameLayot;
    private ViewPager viewPager;
    private ViewPager viewPager_group;
    private BottomNavigationView bottomNavigation;
    private Toolbar toolbar;

    Dialog dialog;
    Button cancel_btn, confirm_btn;
    TextView title, description;

    Fragment fragment;
    android.support.v4.app.FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getApplicationContext();
        extras = getIntent().getExtras();

        userID = Long.parseLong(readFromPreferences(getApplicationContext(), USER_ID, "-1"));
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.user_panel_tab_layout, contentFrameLayout);

        setTabLayout();

        bottomNavigation = (BottomNavigationView) findViewById(R.id.bottom_navigation);

        fragment = new NotificationsFragment();
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
        frameLayot = (FrameLayout) findViewById(R.id.fragmentContainer);
        frameLayot.setVisibility(View.VISIBLE);

        bottomNavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.bottom_navigation_notes:
                               // setToolbarTittle("Oceny");
                                viewPager.setVisibility(View.VISIBLE);
                                tabLayout.setVisibility(View.VISIBLE);

                                frameLayot.setVisibility(View.INVISIBLE);
                                tabLayout_group.setVisibility(View.INVISIBLE);
                                viewPager_group.setVisibility(View.INVISIBLE);

                                break;
                            case R.id.bottom_navigation_notifications:
                              // setToolbarTittle("Powiadomienia");
                                viewPager.setVisibility(View.INVISIBLE);
                                tabLayout.setVisibility(View.INVISIBLE);
                                tabLayout_group.setVisibility(View.INVISIBLE);
                                viewPager_group.setVisibility(View.INVISIBLE);

                                frameLayot.setVisibility(View.VISIBLE);
                                break;
                            case R.id.bottom_navigation_groups:
                                viewPager.setVisibility(View.INVISIBLE);
                                tabLayout.setVisibility(View.INVISIBLE);
                                frameLayot.setVisibility(View.INVISIBLE);

                                tabLayout_group.setVisibility(View.VISIBLE);
                                viewPager_group.setVisibility(View.VISIBLE);
                                break;
                            default:
                                viewPager.setVisibility(View.INVISIBLE);
                                tabLayout.setVisibility(View.INVISIBLE);
                                tabLayout_group.setVisibility(View.INVISIBLE);
                                viewPager_group.setVisibility(View.INVISIBLE);

                               // fragment = new EventDetailsFragment_Comments();
                               // fragmentManager = getSupportFragmentManager();
                               // fragmentManager.beginTransaction().replace(R.id.fragmentContainer, fragment).commit();
                                break;
                        }
                        return true;
                    }
                });

    }

    private void setTabLayout() {
        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        Notes_FPAdapter adapter = new Notes_FPAdapter(this, getSupportFragmentManager(), extras);
        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
        
        tabLayout_group = (TabLayout) findViewById(R.id.sliding_tabs_groups);
        viewPager_group = (ViewPager) findViewById(R.id.viewpager_groups);
        Groups_FPAdapter adapter1 = new Groups_FPAdapter(this, getSupportFragmentManager(), extras);
        viewPager_group.setAdapter(adapter1);
        tabLayout_group.setupWithViewPager(viewPager_group);
    }

}
