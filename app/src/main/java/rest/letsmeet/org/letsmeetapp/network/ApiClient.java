package rest.letsmeet.org.letsmeetapp.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.LocalTime;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import rest.letsmeet.org.letsmeetapp.json.LocalTimeDeserializer0;
import rest.letsmeet.org.letsmeetapp.json.LocalTimeSerializer0;
import rest.letsmeet.org.letsmeetapp.json.TimeDeserializer;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

  //public static final String BASE_URL = "http://10.0.2.2:8080/";
  //public static final String BASE_URL = "http://156.17.37.147:8080/";
   // public static final String BASE_URL = "http://192.168.1.7:8080/";
  public static final String BASE_URL = "http://192.168.43.145:8080/";

    //public static final String BASE_URL = "https://letsmeetapp-web-service.herokuapp.com/";

    private static Retrofit retrofit = null;

    public static Retrofit getRetrofit(){
        GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd");

        // add custom deserializers
        gsonBuilder.registerTypeAdapter(LocalTime.class, new TimeDeserializer());

        //
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
             //   .addInterceptor(loggingInterceptor)
                .build();



        Gson myGson = gsonBuilder
                .registerTypeAdapter(LocalTime.class, new LocalTimeSerializer0())
                .registerTypeAdapter(LocalTime.class, new LocalTimeDeserializer0())
                .create();

        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(myGson))
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
