package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.joda.time.LocalDate;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.EventsRVAdapter;
import rest.letsmeet.org.letsmeetapp.model.Event;

public class EventsFragment_Today extends EventsFragment {

    public EventsFragment_Today() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            groupID = extras.getLong("groupID");
            userID = extras.getLong("userID");
        }else{
            groupID = -1;
            userID = -1;
        }
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.group_fragment, container, false);

        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv);
        rv.setLayoutManager(llm);

        if(groupID != -1){
            getEventsList(groupID);
        }
        else{
            Log.d("EventsFragments_Today", "Nie przekazano id grupy");
        }

        return rootView;
    }

    public void showEvents(List<Event> events) {
        LocalDate date_curr = new LocalDate();
        LocalDate date_event_begin;
        LocalDate date_event_end;

        Iterator<Event> iterator = events.iterator();
        while(iterator.hasNext()){
            Event event = iterator.next();
            date_event_begin = new LocalDate(event.getBeginningDate().getTime());
            date_event_end = new LocalDate(event.getEndingDate().getTime());

            if(!date_event_end.equals(date_curr) &&  !date_event_begin.equals(date_curr)){
                iterator.remove();
            }
        }

        Collections.sort(events, new Comparator<Event>() {
            public int compare(Event o1, Event o2) {
                if (o1.getBeginningDate() == null || o2.getBeginningDate() == null)
                    return 0;
                return o1.getBeginningDate().compareTo(o2.getBeginningDate());
            }
        });

        rva = new EventsRVAdapter(getContext(), events, userID);
        rv.setAdapter(rva);
    }


}
