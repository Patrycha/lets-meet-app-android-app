package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.tab.ParticipantFragment_Accepted;
import rest.letsmeet.org.letsmeetapp.tab.ParticipantFragment_Declined;
import rest.letsmeet.org.letsmeetapp.tab.ParticipantFragment_Invited;
import rest.letsmeet.org.letsmeetapp.tab.ParticipantFragment_Pending;

public class EventDetailsFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private Bundle bundle;

    public EventDetailsFragmentPagerAdapter(Context context, FragmentManager fm, Bundle bundle) {
        super(fm);
        this.mContext = context;
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if(position == 0)
            fragment = new ParticipantFragment_Accepted();
        else if(position == 1)
            fragment = new ParticipantFragment_Declined();
        else if(position == 2)
            fragment = new ParticipantFragment_Invited();
        else
            fragment = new ParticipantFragment_Pending();

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        String[] titles = mContext.getResources().getStringArray(R.array.participants_tabs);

        switch (position) {
            case 0:
                return titles[0];
            case 1:
                return titles[1];
            case 2:
                return titles[2];
            case 3:
                return titles[3];
            default:
                return null;
        }
    }
}
