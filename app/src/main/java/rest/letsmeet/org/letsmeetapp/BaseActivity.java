package rest.letsmeet.org.letsmeetapp;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_LOGIN;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_NAME;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_SURNAME;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.readFromPreferences;


public class BaseActivity extends AppCompatActivity{
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout_with_navigation);

        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);

        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        View header = navigationView.getHeaderView(0);
        TextView fullName = (TextView) header.findViewById(R.id.txtUsername);
        TextView login = (TextView) header.findViewById(R.id.txtUserLogin);
        String name = readFromPreferences(getApplicationContext(), USER_NAME, "");
        String surname = readFromPreferences(getApplicationContext(), USER_SURNAME, "");
        String login2 = readFromPreferences(getApplicationContext(), USER_LOGIN, "");

        fullName.setText(name + " " + surname);
        login.setText(login2);
        //TODO: use Picasso library


        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                Intent anIntent = null;
                ActivityManager am = (ActivityManager)getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                String classNameWithPackages = cn.getClassName();
                int index = classNameWithPackages.lastIndexOf('.');
                String className = classNameWithPackages.substring(index+1);

                switch (item.getItemId()) {
                    case R.id.item1:
                        if(!className.equals("CategoriesActivity")){
                            anIntent = new Intent(getApplicationContext(), CategoriesActivity.class);
                            startActivity(anIntent);
                        }
                        break;
                    case R.id.item2:
                        if(!className.equals("UserPanelActivity")) {
                            anIntent = new Intent(getApplicationContext(), UserPanelActivity.class);
                            startActivity(anIntent);
                        }
                        break;
                    case R.id.item4:
                        if(!className.equals("LoginActivity")) {
                            anIntent = new Intent(getApplicationContext(), LoginActivity.class);
                            startActivity(anIntent);
                        }
                        break;
                    default:
                        break;
                }

                drawerLayout.closeDrawers();
                return false;
            }
        });

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        actionBarDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
            drawerLayout.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }
}
