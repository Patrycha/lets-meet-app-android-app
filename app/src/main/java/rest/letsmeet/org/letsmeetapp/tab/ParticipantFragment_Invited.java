package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_INVITED;

public class ParticipantFragment_Invited extends ParticipantFragment {


    public ParticipantFragment_Invited() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = PARTICIPATION_INVITED;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }


}
