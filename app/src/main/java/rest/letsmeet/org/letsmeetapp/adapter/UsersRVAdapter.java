package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.User;


public class UsersRVAdapter extends RecyclerView.Adapter<UsersRVAdapter.UserViewHolder>{
    private long user_id = 2;

    List<User> users;
    Context context;

    public UsersRVAdapter(Context context, List<User> users){
        this.context = context;

        if(users != null) this.users = users;
        else this.users = new ArrayList<>();

        //TODO userID
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_card, parent, false);
        UserViewHolder gvh = new UserViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(UserViewHolder holder, final int position) {
        holder.userName.setText(users.get(position).getName() + " " + users.get(position).getSurname());

    /*        holder.joinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  joinOrLeaveGroup(user_id, users.get(position).getId());
            }
        });
        holder.detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle extras = new Bundle();
            //    extras.putLong("eventID", users.get(position).getId());
                extras.putLong("userID",user_id);
               // Intent intent = new Intent(context, EventsTabsActivity.class);
               // intent.putExtras(extras);
               // context.startActivity(intent);
            }
        });
        */
    }


    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder{

        TextView userName;
     //   Button joinButton;
     //   Button detailButton;


        public  UserViewHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.user_name);
     //       joinButton = (Button) itemView.findViewById(R.id.event_join_btn);
      //      detailButton = (Button) itemView.findViewById(R.id.event_details_btn);
        }
    }
}
