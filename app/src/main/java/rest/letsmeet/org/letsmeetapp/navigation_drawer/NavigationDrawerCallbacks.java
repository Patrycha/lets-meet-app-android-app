package rest.letsmeet.org.letsmeetapp.navigation_drawer;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
