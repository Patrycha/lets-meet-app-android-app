package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.Notification;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.NOTIFICATION_CANCELLED;

public class NotificationRVAdapter extends RecyclerView.Adapter<NotificationRVAdapter.NotificationViewHolder>{
    private List<Notification> list;
    private long userID;
    private Context context;

    public NotificationRVAdapter(Context context, long userId, List<Notification> list) {
        this.context = context;
        userID = userId;
        this.list = list;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_card, parent, false);
        NotificationViewHolder gvh = new NotificationViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, final int position) {
        Notification el = list.get(position);
        String text = "Do grupy " + el.getEvent().getGroupName() +
                " zostało dodane nowe wydarzenie " + el.getEvent().getName();

        holder.notificationMessage.setText(text);
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelNotification(list.get(position).getId(), position);
            }
        });
    }

    private void cancelNotification(long notificationId, final int position) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Notification> call = service.cancelNotification(notificationId);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<Notification>() {
                @Override
                public void onResponse(Call<Notification> call, Response<Notification> response) {

                    if(response.isSuccessful()){
                        if(response.body().getStatus().getName().equals(NOTIFICATION_CANCELLED)) {
                            list.remove(position);
                            notifyDataSetChanged();
                        }
                    }
                }
                @Override
                public void onFailure(Call<Notification> call, Throwable t) {
                }
            });
        }catch (Exception e){
            Log.e("Error in cancelNotif...",e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public void updateDataSet(List<Notification> list) {
        if(this.list == null){
            this.list = new ArrayList<>();
        }
        this.list.addAll(list);
        //this.list = list;
    }

    public class NotificationViewHolder extends RecyclerView.ViewHolder{
        private TextView notificationMessage;
        private ImageButton deleteButton;

        public NotificationViewHolder(View itemView){
            super(itemView);
            notificationMessage = (TextView) itemView.findViewById(R.id.notification_textview);
            deleteButton = (ImageButton) itemView.findViewById(R.id.notification_delete_imgbtn);
        }
    }
}
