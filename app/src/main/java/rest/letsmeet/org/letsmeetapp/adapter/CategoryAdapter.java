package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.Category;

public class CategoryAdapter extends BaseAdapter {

    List<Category> list;
    Context context;

    public CategoryAdapter(Context context, List<Category> list){
        this.list = list;
        this.context = context;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).getId();
    }

    class ViewHolder{
        TextView textView;
        ImageView imageView;
        ViewHolder(View v){
            imageView = (ImageView) v.findViewById(R.id.category_image);
            textView = (TextView) v.findViewById(R.id.category_name_textview);
        }
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View row = view;
        ViewHolder viewHolder = null;

        if (row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.category_item, parent, false);

            viewHolder = new ViewHolder(row);
            row.setTag(viewHolder); //TODO: wczesniej był błąd, gdy było view.setTag
        }
        else{
            viewHolder = (ViewHolder) view.getTag();
        }
        //TextView textView = (TextView) row.findViewById(R.id.category_name_textview);
        viewHolder.textView.setText(list.get(position).getName());

        Resources resources = context.getResources();
        String photoPath = list.get(position).getPhotoPath();
        if(photoPath != null) {
            final int resourceId = resources.getIdentifier(list.get(position).getPhotoPath(), "drawable", context.getPackageName());
            viewHolder.imageView.setImageResource(resourceId);//(R.drawable.ic_theaters_white_48dp);
        }
      /*
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.category_item, parent, false);
        TextView textView = (TextView) row.findViewById(R.id.category_name_textview);
        textView.setText(list.get(position).getName());

        //
        ImageView imageView1 = (ImageView)row.findViewById(R.id.category_image);
        imageView1.setImageResource(R.drawable.ic_theaters_white_48dp);

        */


        return row;
    }
}