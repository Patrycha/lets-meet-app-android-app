package rest.letsmeet.org.letsmeetapp.busevent;

public class NewGroupEvent {

    private final String message;

    public NewGroupEvent(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
