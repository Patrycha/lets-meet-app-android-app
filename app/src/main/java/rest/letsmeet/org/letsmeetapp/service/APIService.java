package rest.letsmeet.org.letsmeetapp.service;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.model.Category;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.model.Group;
import rest.letsmeet.org.letsmeetapp.model.Notification;
import rest.letsmeet.org.letsmeetapp.model.Participation;
import rest.letsmeet.org.letsmeetapp.model.ParticipationStatus;
import rest.letsmeet.org.letsmeetapp.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {//endpoints

    @GET("category/")
    Call<List<Category>> getCategories();

    @GET("category/{id}/group/")
    Call<List<Group>> getGroups(@Path("id") long categoryID);

    @GET("group/")
    Call<List<Group>> getGroups();

    @GET("group/{gid}/")
    Call<Group> getGroup(@Path("gid") long groupId);

    @GET("user/{uid}/subscribing/{gid}/")
    Call<Boolean> isSubscribing(@Path("uid") long userID, @Path("gid") long groupID);

    @PUT("user/{uid}/subscribe/{gid}/")
    Call<Void> setSubsciption(@Path("uid") long userId, @Path("gid") long groupId);

    @DELETE("user/{uid}/unsubscribe/{gid}/")
    Call<Boolean> unsetSubscribtion(@Path("uid") long userId, @Path("gid") long groupId);

    @GET("user/{uid}/groups/")
    Call<List<Group>> getUserGroups(@Path("uid") long userId, @Query("category") Long categoryId);

    @PUT("category/{cid}/group/")
    Call<Void> createGroup(@Path("cid") long categoryId, @Body Group group, @Query("userId") long userId);

    @GET("group/{gid}/event/")
    Call<List<Event>> getEvents(@Path("gid") long groupId);

    @GET("group/{gid}/user/")
    Call<List<User>> getUsers(@Path("gid") long groupId);

    @POST("event/{eid}/user/{uid}/")
    Call<ParticipationStatus> setParticipation(@Path("eid") long eventId, @Path("uid") long userId);

    @GET("user/{uid}/participating/{eid}/")
    Call<ParticipationStatus> getParticipationStatus(@Path("eid") long eventId, @Path("uid") long userId);

    @GET("group/{gid}/subscriber/count/")
    Call<Integer> getSubscribersCount(@Path("gid") long groupId);

    @GET("group/{gid}/event/count/")
    Call<Integer> getEventsCount(@Path("gid") long groupId);

    @GET("event/{eid}/")
    Call<Event> getEvent(@Path("eid") long eventId);

    @GET("event/{eid}/participation/user/{uid}/")
    Call<Participation> getParticipation(@Path("uid") long userID, @Path("eid") long eventID);//todo: test

    @GET("event/{eid}/user/")
    Call<List<User>> getParticipants(@Path("eid") long eventId, @Query("status") String status);

    @POST("group/{gid}/user/{uid}/")
    Call<Boolean> addNewEvent(@Path("uid") long userID,@Path("gid") long groupID, @Body Event event);

    @GET("user/{uid}/")
    Call<User> getUser(@Path("uid") long userID);

    @GET("category/{cid}/")
    Call<Category> getCategory(@Path("cid") long categoryID);

    @POST("event/{eid}/user/{uid}/changestatus/")
    Call<ParticipationStatus> changeParticipationStatus(@Path("uid") long userId, @Path("eid") long eventId, @Query("changer") long userChangeID, @Query("status") String status);

    @GET("event/{eid}/user/{uid}/isAdmin/")
    Call<Boolean> isEventAdmin(@Path("eid") long eventId, @Path("uid") long userId);

    @GET("user/{uid}/grade/")
    Call<List<Participation>> participationsWithRaterToGrade(@Path("uid") long userID);

    @GET("user/{uid}/gradeEvents/")
    Call<List<Event>> eventsToGrade(@Path("uid") long userID);

    @GET("login/")
    Call<User> getUser(@Header("Authorization") String header);

    @POST("register/")
    Call<User> register(@Body User user);

    @GET("user/{uid}/organizer/{gid}/")
    Call<Boolean> isOrganizer(@Path("uid") long userId, @Path("gid") long groupId);

    @GET("user/{uid}/notification/")
    Call<List<Notification>> getNotifications (@Path("uid") long userId, @Query("status") String statusName);

    @GET("notification/{nid}/cancel/")
    Call<Notification> cancelNotification(@Path("nid") long notificationId);

    @GET("user/{uid}/group/")
    Call<List<Group>> getGroups(@Path("uid") long userID,
                                @Query("isAdmin") boolean isAdmin,
                                @Query("isSubscriber") boolean isSubscriber,
                                @Query("categoryID") long categoryId);

}
