package rest.letsmeet.org.letsmeetapp.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.google.gson.annotations.Expose;

import org.joda.time.LocalTime;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Event implements Serializable{

    @Expose
    private long id;
    @Expose
    private String name;
    @Expose
    private String description;
    @Expose
    private String place;
    @Expose
    private Date beginningDate;

    @Expose
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
   // @JsonSerialize(using = LocalTimeSerializer0.class)
    //@JsonDeserialize(using = LocalTimeDeserializer0.class)
    //@JsonSerialize(using = LocalTimeSerializer0.class)
    private LocalTime beginningTime;
    @Expose
    private Date endingDate;

    @Expose
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
   // @JsonSerialize(using = LocalTimeSerializer0.class)
   // @JsonSerialize(using = LocalTimeSerializer0.class)
    private LocalTime endingTime;

    @Expose
    private Date creatingDate;
    @Expose
    private String imagePath;
    @Expose
    private int participantsLimit;

    private int participationCounter;
    @Expose
    private VisibilityStatus visibility;
    @Expose
    private Group group;
    @Expose
    private User organizer;

    Event(){

    }

    public Event(long id, String n, String d){
        this.id = id;
        name = n;
        description = d;
        creatingDate = new Date();
    }

    public Event(String n, String d, String p, Date start, Date end){
        this.id = 0;
        name = n;
        description = d;
        place = p;
        beginningDate = start;
        endingDate = end;
        creatingDate = new Date();
    }

    public long getId() {return id;}

    public String getName() {
        return name;
    }

    public String getPlace(){return place;}

    public int getParticipantsLimit() {
        return participantsLimit;
    }

    public int getAcceptedParticipantCounter() {
        return participationCounter;
    }

    public String getDescription() {
        return description;
    }

    public Date getBeginningDate() {
        return beginningDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public VisibilityStatus getVisibility() {
        return visibility;
    }

    public Group getGroup() {
        return group;
    }

    public User getOrganizer() {
        return organizer;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public void setBeginningDate(Date beginningDate) {
        this.beginningDate = beginningDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public void setParticipantsLimit(int participantsLimit) {
        this.participantsLimit = participantsLimit;
    }

    public void setVisibility(VisibilityStatus visibility) {
        this.visibility = visibility;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setOrganizer(User organizer) {
        this.organizer = organizer;
    }

    public LocalTime getBeginningTime() {
        return beginningTime;
    }

    public void setBeginningTime(LocalTime beginningTime) {
        this.beginningTime = beginningTime;
    }

    public LocalTime getEndingTime() {
        return endingTime;
    }

    public void setEndingTime(LocalTime endingTime) {
        this.endingTime = endingTime;
    }

    public String getImagePath() {
        return imagePath;
    }

    private String groupName;

    public String getGroupName(){return groupName;}

    private String organizerNameSurname;

    public String getOrganizerNameSurname(){return organizerNameSurname;}

    private long organizerId;

    public long getOrganizerId(){return organizerId;}

    public boolean isOver() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        Date currentDate = cal.getTime();
        LocalTime currentTime = new LocalTime();

        if(endingDate.before(currentDate) ||
                (endingDate.equals(currentDate) && endingTime.isBefore(currentTime)))
            return true;

        return false;
    }
}
