package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.PresenceRVAdapter;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.model.Participation;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;

public class NotesFragment_Presence extends android.support.v4.app.Fragment {
    private long userID;
    private RecyclerView rv ;
    private LinearLayoutManager llm;
    private PresenceRVAdapter adapter;

    public NotesFragment_Presence(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null)
            userID = extras.getLong(USER_ID);
        else
            userID = -1;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.presence_fragment, container, false);
        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv);
        rv.setLayoutManager(llm);

        if(userID != -1){
            //action
           // getParticipantsToGradeAndYours(userID);
            getEvents(userID);
        }

        return rootView;
    }

    private void getParticipantsToGradeAndYours(final long userID) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            //Call<List<User>> call = service.getParticipants1( status, eventID);
            Call<List<Participation>> call = service.participationsWithRaterToGrade(userID);
            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<Participation>>() {
                @Override
                public void onResponse(Call<List<Participation>> call, Response<List<Participation>> response) {

                    if(response.code() == 200){
                        if(response.body() != null){
                           // showUsers(userID, response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Participation>> call, Throwable t) {
                   // Toast.makeText(getContext(), "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.e("Error in getEventList",e.getMessage());
        }
    }
/*
    private void showUsers(long userID, List<Participation> list) {
        adapter = new PresenceRVAdapter(getContext(), list);
        rv.setAdapter(adapter);
    }
*/
    //
    private void getEvents(final long userID) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);

            Call<List<Event>> call = service.eventsToGrade(userID);
            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<Event>>() {
                @Override
                public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {

                    if(response.code() == 200){
                        if(response.body() != null){
                            showUsers(userID, response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Event>> call, Throwable t) {
                    Log.d("getEvents", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getEventList",e.getMessage());
        }
    }

    private void showUsers(long userID, List<Event> list) {
        adapter = new PresenceRVAdapter(getContext(), list);
        rv.setAdapter(adapter);
    }
}
