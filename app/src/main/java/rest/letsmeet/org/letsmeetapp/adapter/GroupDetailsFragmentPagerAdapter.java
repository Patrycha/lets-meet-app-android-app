package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rest.letsmeet.org.letsmeetapp.tab.GroupDetailsFragment_Details;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.GROUP_DESCR;

public class GroupDetailsFragmentPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private String groupDetails;
    private long groupAdminID;


    public GroupDetailsFragmentPagerAdapter(Context context, FragmentManager fm, String groupDescription, long groupAdminID) {
        super(fm);
        this.mContext = context;
        groupDetails = groupDescription;
        this.groupAdminID = groupAdminID;
    }

    @Override
    public Fragment getItem(int position) {

        GroupDetailsFragment_Details fragment = new GroupDetailsFragment_Details();

        Bundle extras = new Bundle();
        extras.putString(GROUP_DESCR, groupDetails);
        extras.putLong("groupAdminID", groupAdminID);
        fragment.setArguments(extras);

        return fragment;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }
}
