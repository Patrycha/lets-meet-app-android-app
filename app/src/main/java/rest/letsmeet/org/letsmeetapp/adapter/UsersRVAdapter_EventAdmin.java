package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.busevent.ChangeParticipationStatusEvent;
import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.ParticipationStatus;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_ACCEPTED;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_DECLINED;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_INVITED;

public class UsersRVAdapter_EventAdmin extends RecyclerView.Adapter<UsersRVAdapter_EventAdmin.UserViewHolder>{
    private long userID;
    private long eventID;
    private String status;
    private List<User> users;
    private Context context;

    public UsersRVAdapter_EventAdmin(Context context, List<User> users, long eventId, long userId, String status){
        this.context = context;
        this.eventID = eventId;
        this.userID = userId;
        this.status = status;

        if(users != null)
            this.users = users;
        else
            this.users = new ArrayList<>();
    }

    @Override
    public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_card_eventadmin, parent, false);
        UserViewHolder gvh = new UserViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {
        holder.userName.setText(users.get(position).getName() + " " + users.get(position).getSurname());

        if ( status.equals(PARTICIPATION_DECLINED)){
            holder.declineButton.setClickable(false);
            holder.declineButton.setVisibility(View.INVISIBLE);
            holder.declineButton.setMaxWidth(0);
        }
        if ( status.equals(PARTICIPATION_ACCEPTED) || status.equals(PARTICIPATION_INVITED)){
            holder.acceptButton.setClickable(false);
            holder.acceptButton.setVisibility(View.INVISIBLE);
            holder.acceptButton.setMaxWidth(0);
        }
        holder.acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeParticipationStatus(users.get(position).getId(), userID, PARTICIPATION_ACCEPTED ,eventID, position);
              //  holder.acceptButton.setClickable(false);
               // holder.acceptButton.setImageAlpha(70);
              //  holder.declineButton.setClickable(true);
              //  holder.declineButton.setImageAlpha(255);
            }
        });
        holder.declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeParticipationStatus(users.get(position).getId(), userID, PARTICIPATION_DECLINED, eventID, position);
              //  holder.declineButton.setClickable(false);
              //  holder.declineButton.setImageAlpha(70);
              //  holder.acceptButton.setClickable(true);
              //  holder.acceptButton.setImageAlpha(255);
            }
        });
    }

    private void changeParticipationStatus(long userId, long userChangeID, final String status, long eventId, final int position) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<ParticipationStatus> call = service.changeParticipationStatus(userId, eventId, userChangeID, status);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<ParticipationStatus>() {
                @Override
                public void onResponse(Call<ParticipationStatus> call, Response<ParticipationStatus> response) {

                    if(response.code() == 200){
                        if(response.body() != null){
                            if(status.equals(response.body().getName())){
                                Log.d(String.valueOf(Log.INFO), "new participation status was set correct :)");
                            }else{
                                Log.d(String.valueOf(Log.INFO), "new participation status was not set correct :(");
                            }
                        }
                        users.remove(position);
                        notifyDataSetChanged();
                        EventBus.getDefault().post(new ChangeParticipationStatusEvent("changed"));
                    }else if(response.code() == 404){
                        Log.d("changeP.Status", "resources not found");
                    }else if(response.code() == 409){
                        Toast.makeText(context, "Osiągnięto limit uczestników!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ParticipationStatus> call, Throwable t) {
                    Log.w("changeP.Status","onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in changeP.Status",e.getMessage());
        }
    }


    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder{

        TextView userName;
        ImageButton acceptButton;
        ImageButton declineButton;

        public  UserViewHolder(View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.user_name);
            acceptButton = (ImageButton) itemView.findViewById(R.id.participant_accept_btn);
            declineButton = (ImageButton) itemView.findViewById(R.id.participant_decline_btn);
        }
    }
}
