package rest.letsmeet.org.letsmeetapp.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class VisibilityStatus implements Serializable{
    @Expose
    private long id;
    @Expose
    private String name;

    VisibilityStatus(){}

    public VisibilityStatus(long id, String name){
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
