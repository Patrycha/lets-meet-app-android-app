package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailsFragment_Details extends android.support.v4.app.Fragment {
    private long userID;
    private long eventID;

    TextView description;
    TextView place;
    TextView start;
    TextView end;
    TextView organizer;

    public EventDetailsFragment_Details(){
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            eventID = extras.getLong("eventID");
            userID = extras.getLong("userID");
        }else{
            eventID = -1;
            userID = -1;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.event_details_fragment, container, false);
        description = (TextView) rootView.findViewById(R.id.event_details_tv);
        place = (TextView) rootView.findViewById(R.id.event_place_tv);
        start = (TextView) rootView.findViewById(R.id.event_start_tv);
        end = (TextView) rootView.findViewById(R.id.event_end_tv);
        organizer = (TextView) rootView.findViewById(R.id.event_organizer_tv);

        if(eventID != -1){
            getEvent(eventID, description, place, start, end, organizer);

        }
        else{
            //Toast.makeText(getActivity(), "nie przekazano id wydarzenia", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }


    private void getEvent(long eventId, final TextView description, final TextView place, final TextView start, final TextView end, final TextView organizer) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            final Call<Event> call = service.getEvent(eventId);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<Event>() {
                @Override
                public void onResponse(Call<Event> call, Response<Event> response) {
                    if(response.code() == 200){
                        if(response.body() == null){
                           // Toast.makeText(getContext(), "Body is null", Toast.LENGTH_SHORT).show();
                        }else{
                            Event event = response.body();

                            DateTimeFormatter dtf = DateTimeFormat.forPattern("dd-MM-YYYY");
                            String formatedDate = dtf.print(new LocalDate(event.getBeginningDate()));
                            String event_start = formatedDate + " ";
                            event_start += (new LocalTime(event.getBeginningTime())).toString().substring(0, 5);
                            start.setText(event_start);

                            String formatedDate_end = dtf.print(new LocalDate(event.getEndingDate()));
                            String event_end = formatedDate_end + " ";
                            event_end += (new LocalTime(event.getEndingTime())).toString().substring(0, 5);
                            end.setText(event_end);

                            description.setText(event.getDescription());
                            place.setText(event.getPlace());
                            organizer.setText(event.getOrganizerNameSurname());
                        }
                    }
                }
                @Override
                public void onFailure(Call<Event> call, Throwable t) {
                    Log.d("getEvent", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getEvent",e.getMessage());
        }
    }
}
