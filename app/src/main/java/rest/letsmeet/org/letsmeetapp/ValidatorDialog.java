package rest.letsmeet.org.letsmeetapp;


import android.app.Dialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

public class ValidatorDialog extends Dialog implements Validator.ValidationListener, View.OnClickListener {

    protected Validator validator;
    protected boolean validated;
    Context context;

    ValidatorDialog(Context context, int themeResId){
        super(context,themeResId);
        this.context = context;
        validator = new Validator(context);
        validator.setValidationListener(this);
    }

    protected boolean validate() {
        if (validator != null)
            validator.validate();
        return validated;
    }

    @Override
    public void onClick(View v) {
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        validated = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        validated = false;

        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(context);

            // Display error messages
            if (view instanceof Spinner) {
                Spinner sp = (Spinner) view;
                view = ((LinearLayout) sp.getSelectedView()).getChildAt(0);        // we are actually interested in the text view spinner has
            }

            if (view instanceof TextView) {
                TextView et = (TextView) view;
                et.setError(message);
            }
        }
    }

    @Override
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> data, @Nullable Menu menu, int deviceId) {

    }
}
