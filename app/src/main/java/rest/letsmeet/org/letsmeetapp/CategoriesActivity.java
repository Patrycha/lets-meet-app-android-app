package rest.letsmeet.org.letsmeetapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.adapter.CategoryAdapter;
import rest.letsmeet.org.letsmeetapp.adapter.CategoryAdapter2;
import rest.letsmeet.org.letsmeetapp.model.Category;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.readFromPreferences;

public class CategoriesActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private long userID;

    //NavigationDrawerFragment navigationDrawer;
    GridView gridView;
    CategoryAdapter2 adapter;
    RecyclerView rv;
    CategoryAdapter adapter2;

    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        userID = Long.parseLong(readFromPreferences(getApplicationContext(), USER_ID, "-1"));

        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame); //Remember this is the FrameLayout area within your activity_main.xml
        getLayoutInflater().inflate(R.layout.categories, contentFrameLayout);

        gridView = (GridView) findViewById(R.id.categories_gridview);

        setAdapter(this);
        gridView.setOnItemClickListener(this);

    }

    private void setAdapter(final Context context) {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<Category>> call = service.getCategories();

            call.enqueue(new Callback<List<Category>>() {
                @Override
                public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                    List<Category> list = response.body();

                    adapter2 = new CategoryAdapter(context, list);
                    gridView.setAdapter(adapter2);

                }

                @Override
                public void onFailure(Call<List<Category>> call, Throwable t) {
                    //Toast.makeText(getApplicationContext(), "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Log.e("Error in fetching cat.", e.getMessage());
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle bundle = new Bundle();
        bundle.putLong("userID", userID);
        bundle.putLong("categoryID", id);

        Intent intent = new Intent(this, GroupsTabsActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}

