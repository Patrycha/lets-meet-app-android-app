package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.tab.NotesFragment_MyNotes;
import rest.letsmeet.org.letsmeetapp.tab.NotesFragment_Notes;
import rest.letsmeet.org.letsmeetapp.tab.NotesFragment_Presence;

public class Notes_FPAdapter extends FragmentPagerAdapter {
    private Context mContext;
    private Bundle bundle;

    public Notes_FPAdapter(Context context, FragmentManager fm, Bundle bundle) {
        super(fm);
        this.mContext = context;
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        if(position == 0) {
            fragment = new NotesFragment_Presence();
        }
        else if(position == 1) {
            fragment = new NotesFragment_Notes();
        }
        else {
            fragment = new NotesFragment_MyNotes();
        }

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String[] titles = mContext.getResources().getStringArray(R.array.userpanel_notes_tabs);

        switch (position) {
            case 0:
                return titles[0];
            case 1:
                return titles[1];
            case 2:
                return titles[2];
            default:
                return null;
        }
    }
}
