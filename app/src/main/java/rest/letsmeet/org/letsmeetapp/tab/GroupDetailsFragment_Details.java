package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.GROUP_DESCR;

public class GroupDetailsFragment_Details extends android.support.v4.app.Fragment {
   // private long userID;
    private long groupID;
    private String groupDetails;
    private long groupAdminID;

    private TextView description;
    private TextView admin;

    public GroupDetailsFragment_Details(){
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getArguments();//getActivity().getIntent().getExtras();//to jest inny obiekt Bundle
        if(extras != null) {
            groupDetails = extras.getString(GROUP_DESCR);
            groupAdminID = extras.getLong("groupAdminID");
        }else{
            groupDetails = "Brak szczegółów dotyczących grupy.";
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.group_details_fragment, container, false);
        description = (TextView) rootView.findViewById(R.id.group_details_tv);
        admin = (TextView) rootView.findViewById(R.id.group_admin_tv);
        description.setText(groupDetails);
        setAdminTextView(groupAdminID, admin);

        return rootView;
    }

    private void setAdminTextView(long userID, final TextView textView) {
        try {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<User> call = service.getUser(userID);

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {

                    if (response.code() == 200) {
                        User user = response.body();
                        if ( user != null) {
                            textView.setText(user.getName() + " " + user.getSurname());
                        }
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    //Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            Log.e("Error in getGroup", e.getMessage());
        }
    }


}
