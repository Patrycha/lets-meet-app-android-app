package rest.letsmeet.org.letsmeetapp.tab;

import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.adapter.EventsRVAdapter;
import rest.letsmeet.org.letsmeetapp.busevent.ChangeParticipationStatusEvent;
import rest.letsmeet.org.letsmeetapp.busevent.NewEventEvent;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EventsFragment extends Fragment {
    protected long userID;
    protected long groupID;

    protected RecyclerView rv ;
    protected LinearLayoutManager llm;
    protected EventsRVAdapter rva;

    public EventsFragment(){
    }

    protected void getEventsList(long groupId) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<Event>> call = service.getEvents(groupId);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<Event>>() {
                @Override
                public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {

                    if(response.code() == 200){
                        if(response.body() == null){
                            Log.d("EventsFragments_History"," getEventsLost: Body is null");
                        }else{
                            showEvents(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Event>> call, Throwable t) {
                    Log.w("EventsFragments_History","getEventsLost: onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getEventList",e.getMessage());
        }
    }

    public void showEvents(List<Event> events) {

        Collections.sort(events, new Comparator<Event>() {
            public int compare(Event o1, Event o2) {
                if (o1.getBeginningDate() == null || o2.getBeginningDate() == null)
                    return 0;
                return o1.getBeginningDate().compareTo(o2.getBeginningDate());
            }
        });

        rva = new EventsRVAdapter(getContext(), events, userID);
        rv.setAdapter(rva);
    }

    @Override
    public void onDetach() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Subscribe
    public void onEvent(NewEventEvent event){
        Log.d("EVENT", "NewEvent");
        getEventsList(groupID);
    }

    @Subscribe
    public void onEvent(ChangeParticipationStatusEvent event){
        Log.d("EVENT", "ChangeParticipationStatus");
        getEventsList(groupID);
    }
}
