package rest.letsmeet.org.letsmeetapp.busevent;

public class NewEventEvent {
    private final String message;

    public NewEventEvent(String message){
        this.message = message;
    }

    public String getMessage(){return message;}
}
