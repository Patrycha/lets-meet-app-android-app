package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.GroupRVAdapter;
import rest.letsmeet.org.letsmeetapp.model.Group;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.readFromPreferences;

public class GroupsFragment_Subscrib extends Fragment {

    long userID;

    RecyclerView rv ;
    LinearLayoutManager llm;
    GroupRVAdapter rva;

    public GroupsFragment_Subscrib() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            userID = extras.getLong(USER_ID);
        }else{
            userID = -1;
        }
        userID = Long.parseLong(readFromPreferences(getContext(), USER_ID, "-1"));

        //Toast.makeText(getContext(), "mySubsc:userID: " + userID, Toast.LENGTH_SHORT).show();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.group_fragment, container, false);

        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv);
        rv.setLayoutManager(llm);

        if(userID != -1){
            getGroupList(userID, false, true);
        }

        return rootView;
    }

    private void getGroupList(final long userId, boolean isAdmin, boolean isSubscriber) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<Group>> call = service.getGroups(userID, isAdmin, isSubscriber, -1);

            Log.d(String.valueOf(Log.INFO), service.getGroups().request().url().toString());

            call.enqueue(new Callback<List<Group>>() {
                @Override
                public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {

                    if(response.code() == 200){
                        showGroups(response.body(), userID);
                    }
                }

                @Override
                public void onFailure(Call<List<Group>> call, Throwable t) {
                    Log.d("getGroupList", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getGroupList",e.getMessage());
        }
    }

    private void showGroups(List<Group> groups, long userId) {
        rva = new GroupRVAdapter(getContext(),groups, userId, false);
        rv.setAdapter(rva);
    }
}
