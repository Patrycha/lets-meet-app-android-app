package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_DECLINED;


public class ParticipantFragment_Declined extends ParticipantFragment {

    public ParticipantFragment_Declined(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = PARTICIPATION_DECLINED;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

}
