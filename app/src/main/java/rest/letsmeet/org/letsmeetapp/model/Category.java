package rest.letsmeet.org.letsmeetapp.model;

import java.util.Set;

public class Category {

    private long id;

    private String name;

    private String description;

  //  private Date creatingDate; //2016/11/16 12:08:43

    private String imagePath;

    private Set<Group> groups;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPhotoPath() {
        return imagePath;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public Category(long id, String name) {
        this.id = id;
        this.name = name;
    }

}
