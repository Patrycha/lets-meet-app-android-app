package rest.letsmeet.org.letsmeetapp.json;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

public class LocalTimeSerializer0 implements JsonSerializer<LocalTime> {
    private static final String PATTERN = "HH:mm:ss";
    final DateTimeFormatter fmt = DateTimeFormat.forPattern(PATTERN);
//
//    @Override
//    public void serialize(LocalTime time, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
//        //jgen.writeString(time.toString("HH:mm:ss"));
//       jgen.writeString(time.getHourOfDay()+":" +time.getMinuteOfHour()+":00");
//
//    }

    @Override
    public JsonElement serialize(LocalTime src, Type typeOfSrc, JsonSerializationContext context) {
        String retVal = fmt.print(src);
        Log.v("LOCALTIME SERIALIZED", retVal);
        return new JsonPrimitive(retVal);    }
}
