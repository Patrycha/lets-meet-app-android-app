package rest.letsmeet.org.letsmeetapp.json;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.lang.reflect.Type;

public class LocalTimeDeserializer0 implements JsonDeserializer<LocalTime>{
    private static final String PATTERN = "HH:mm:ss";
    final DateTimeFormatter fmt = DateTimeFormat.forPattern(PATTERN);

    @Override
    public LocalTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Log.v("LOCALTIME DESERIALIZED",json.toString());
        return fmt.parseLocalTime(json.getAsString());
    }
}
