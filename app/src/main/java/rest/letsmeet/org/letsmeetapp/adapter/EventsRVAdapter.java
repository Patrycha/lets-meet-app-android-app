package rest.letsmeet.org.letsmeetapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import rest.letsmeet.org.letsmeetapp.EventDetailsActivity;
import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.model.Event;
import rest.letsmeet.org.letsmeetapp.model.Participation;
import rest.letsmeet.org.letsmeetapp.model.ParticipationStatus;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.DMY;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.EVENT_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.EVENT_ORGANIZER_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_PENDING;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;


public class EventsRVAdapter extends RecyclerView.Adapter<EventsRVAdapter.GroupViewHolder>{
    private long userID;

    private List<Event> events;
    private Context context;
    private Bundle extras;

    public EventsRVAdapter(Context context, List<Event> events, long userId){
        this.context = context;
        userID = userId;

        if(events != null) this.events = events;
        else this.events = new ArrayList<>();
    }

    @Override
    public GroupViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_card, parent, false);
        GroupViewHolder gvh = new GroupViewHolder(v);
        return gvh;
    }

    @Override
    public void onBindViewHolder(GroupViewHolder holder, final int position) {
        final Event event = events.get(position);
        holder.eventName.setText(event.getName());
        holder.eventPlace.setText(event.getPlace());
        SimpleDateFormat dateFormat = new SimpleDateFormat(DMY);

        String x = event.getBeginningTime() == null ? "?": event.getBeginningTime().toString().substring(0,5);//event.getBeginningTime().getHourOfDay() + ":" + event.getBeginningTime().getMinuteOfHour();
        String y = event.getEndingTime() == null? "?": event.getEndingTime().toString().substring(0,5);
        holder.eventStart.setText("Start: " + dateFormat.format(event.getBeginningDate()) + " " + x);
        holder.eventEnd.setText("Koniec: " + dateFormat.format(event.getEndingDate()) + " " + y);
        holder.participants.setText(event.getAcceptedParticipantCounter() + "/" + event.getParticipantsLimit());

        if(events.get(position).getOrganizerId() == userID){
            holder.joinButton.setClickable(false);
            holder.joinButton.setText(context.getResources().getString(R.string.btn_text_leave));
            holder.joinButton.setTextColor(context.getResources().getColor(R.color.inactiveElement));
        }else if(events.get(position).isOver()) {
            holder.joinButton.setClickable(false);
            holder.joinButton.setText(context.getResources().getString(R.string.btn_text_leave));
            holder.joinButton.setTextColor(context.getResources().getColor(R.color.inactiveElement));
        }else{
            setButton(holder.joinButton, event.getId(), userID, position);
            holder.joinButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setOrUnsetParticipation(userID, events.get(position).getId());
                }
            });
        }

        holder.detailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                extras = new Bundle();
                extras.putLong(EVENT_ID, events.get(position).getId());
                extras.putLong(USER_ID, userID);

               /* APIService service = ApiClient.getRetrofit().create(APIService.class);
                final Call<Boolean> call = service.isEventAdmin(events.get(position).getId(), userID);
                try {
                    Boolean result = call.execute().body();
                    extras.putBoolean(IS_EVENT_ADMIN, result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                */
                extras.putLong(EVENT_ORGANIZER_ID, events.get(position).getOrganizerId());
                Intent intent = new Intent(context, EventDetailsActivity.class);
                intent.putExtras(extras);
                context.startActivity(intent);
            }
        });

    }

    private void setButton(final Button button, long eventId, long userId, final int position) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<Participation> call = service.getParticipation(userId, eventId);

            call.enqueue(new Callback<Participation>() {
                @Override
                public void onResponse(Call<Participation> call, Response<Participation> response) {
                    if(response.isSuccessful()){
                     //   Toast.makeText(context, "currentStatus: " +response.body().getName(), Toast.LENGTH_SHORT).show();
                        if(response.body().getId() == 0) {//nie uczestniczy, nie kandyduje itd.
                            button.setText(context.getResources().getString(R.string.btn_text_join));
                            button.setClickable(true);
                            button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                        }
                        else {
                            String status = response.body().getStatus().getName();
                            if (status.equals(PARTICIPATION_PENDING)) {
                                if(response.body().haveThisUserMadeRecentChange()){
                                    button.setText(context.getResources().getString(R.string.btn_text_leave));
                                    button.setClickable(true);
                                    button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                                }else{
                                    button.setText("ZAAKCEPTUJ");
                                    button.setClickable(true);
                                    button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                                }
                            }
                            else if(status.equals("declined")) {
                                button.setClickable(false);
                                button.setTextColor(context.getResources().getColor(R.color.inactiveElement));
                            }
                            else if(status.equals("accepted")){
                                button.setText(context.getResources().getString(R.string.btn_text_leave));
                                button.setClickable(true);
                                button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                            }
                            else {//cancelled
                                button.setText(context.getResources().getString(R.string.btn_text_join));
                                button.setClickable(true);
                                button.setTextColor(context.getResources().getColor(R.color.secondaryDarkColor));
                            }
                        }
                        notifyItemChanged(position);
                    }else{//if(response.isSuccessful())
                        //niepoprawna odpowiedź
                    }
                }

                @Override
                public void onFailure(Call<Participation> call, Throwable t) {
                    Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e){
            Log.e("Error",e.getMessage());
        }
    }

    private void setOrUnsetParticipation(long userId, long eventId) {
       try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<ParticipationStatus> call = service.setParticipation(eventId, userId);

            //  Log.d(String.valueOf(Log.INFO), service.getGroups().request().url().toString());

            call.enqueue(new Callback<ParticipationStatus>() {
                @Override
                public void onResponse(Call<ParticipationStatus> call, Response<ParticipationStatus> response) {
                    if(response.code() == 200){
                        Toast.makeText(context, "currentStatus: " +response.body().getName(), Toast.LENGTH_SHORT).show();
                    }

                }
                @Override
                public void onFailure(Call<ParticipationStatus> call, Throwable t) {
                    Toast.makeText(context, "onFailure", Toast.LENGTH_SHORT).show();
                }
            });

        }catch (Exception e){
            Log.e("Error in fetching cat.",e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public static class GroupViewHolder extends RecyclerView.ViewHolder{

        TextView eventName;
        TextView eventPlace;
        TextView eventStart;
        TextView eventEnd;
        TextView participants;
        Button joinButton;
        Button detailButton;

        public GroupViewHolder(View itemView) {
            super(itemView);
            eventName = (TextView) itemView.findViewById(R.id.event_name_tv);
            eventPlace = (TextView) itemView.findViewById(R.id.event_place_tv);
            eventStart = (TextView) itemView.findViewById(R.id.event_start_tv);
            eventEnd = (TextView) itemView.findViewById(R.id.event_end_tv);
            participants = (TextView) itemView.findViewById(R.id.users_count_tv);
            joinButton = (Button) itemView.findViewById(R.id.event_join_btn);
            detailButton = (Button) itemView.findViewById(R.id.event_details_btn);
        }
    }
}
