package rest.letsmeet.org.letsmeetapp;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Past;

import org.joda.time.LocalDate;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_IMAGE_PATH;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_LOGIN;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_NAME;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_SURNAME;
import static rest.letsmeet.org.letsmeetapp.constant.Constants.YMD;
import static rest.letsmeet.org.letsmeetapp.shared_preferences.SharedPreferencesHelper.saveToPreferences;

public class SignupActivity extends ValidatorActivity implements AdapterView.OnItemSelectedListener{

    private static final String TAG = "SignupActivity";

    @Length(min = 5, max = 30, message = "Od 5 do 30 znaków")
    @BindView(R.id.input_login) EditText _loginText;

    @Length(min = 3, max = 30, message = "Od 3 do 30 znaków")
    @BindView(R.id.input_name) EditText _nameText;

    @Length(min = 3, max = 30, message = "Od 3 do 30 znaków")
    @BindView(R.id.input_surname) EditText _surnameText;

    @Email(message = "example@gmail.com")
    @BindView(R.id.input_email) EditText _emailText;

    @Password(message = "")
    @Length(min = 7, max = 30, message = "Od 7 do 30 znaków")
    @BindView(R.id.input_password) EditText _passwordText;

    @ConfirmPassword(message = "Hasła nie są takie same")
    @BindView(R.id.input_reEnterPassword) EditText _reEnterPasswordText;

    @BindView(R.id.radiobtn_woman) RadioButton _womanRadioBtn;
    @BindView(R.id.radiobtn_man) RadioButton _manRadioBtn;
    @BindView(R.id.btn_signup) Button _signupButton;
    @BindView(R.id.link_login) TextView _loginLink;
    @BindView(R.id.city_spinner) Spinner _spinner;

    @NotEmpty(message = "Podaj datę urodzenia.")
    @Past(message = "Data musi być z przeszłości. Wymagany format daty: " + YMD, dateFormat = YMD)
    @BindView(R.id.input_birthdate) EditText _birthDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignupActivity.super.onClick(v);
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
            }
        });

        //temp
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.cities_array , android.R.layout.simple_spinner_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        _spinner.setAdapter(adapter);

        _spinner.setOnItemSelectedListener(this);
    }

    public void signup() {
        Log.d(TAG, "Signup");

        if (!validated) {
            return;
        }

        //_signupButton.setEnabled(false);
/*
        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.Theme_AppCompat_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();
*/
        String login = _loginText.getText().toString();
        String name = _nameText.getText().toString();
        String surname = _surnameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();
        String birthDateText = _birthDate.getText().toString();
        char sex = _manRadioBtn.isChecked()? 'M' : 'F';

        SimpleDateFormat formatter = new SimpleDateFormat(YMD);
        Date birthDate = null;
        try {
            birthDate = formatter.parse(birthDateText);
        } catch (ParseException e) {
            birthDate = new LocalDate(2000, 01, 01).toDate(); //default date when exception thrown
        }
        User user = new User();
        user.setLogin(login);
        user.setName(name);
        user.setSurname(surname);
        user.setEmail(email);
        user.setPassword(password);
        user.setBirthDate(birthDate);
        user.setSex(sex);

        SignupUserTask task = new SignupUserTask(user, this);

        task.execute((Void)null);
    }


    public void onSignupSuccess() {
        Toast.makeText(getApplicationContext(), "Zarejestrowano pomyślnie", Toast.LENGTH_SHORT).show();
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);//?
        //finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getApplicationContext(), "Rejestracja nie powiodła się", Toast.LENGTH_SHORT).show();
        _signupButton.setEnabled(true);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public class SignupUserTask extends AsyncTask<Void, Void, Boolean>{
        private Context context;
        private User user;
        private Response<User> response;

        public SignupUserTask(User user, Context context){
            this.context = context;
            this.user = user;
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<User> call = service.register(user);

            try {
                response = call.execute();

                //progressDialog.dismiss();

                if(response.isSuccessful()) {

                    if (response.body() != null)
                        return true;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (response.isSuccessful()){
                onSignupSuccess();
                Intent intent = new Intent(context, CategoriesActivity.class);
                Bundle bundle = new Bundle();
                bundle.putLong(USER_ID, response.body().getId());
                intent.putExtras(bundle);
                saveToPreferences(context, USER_ID, String.valueOf(response.body().getId()));//2nd option
                saveToPreferences(context, USER_NAME, String.valueOf(response.body().getName()));//2nd option
                saveToPreferences(context, USER_SURNAME, String.valueOf(response.body().getSurname()));//2nd option
                saveToPreferences(context, USER_LOGIN, String.valueOf(response.body().getLogin()));//2nd option
                saveToPreferences(context, USER_IMAGE_PATH, String.valueOf(response.body().getImagePath()));//2nd option

                finish();
                startActivity(intent);
            }else{
                onSignupFailed();
            }
        }
    }
}
