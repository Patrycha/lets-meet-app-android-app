package rest.letsmeet.org.letsmeetapp.model;

import java.io.Serializable;
import java.util.List;

public class Group implements Serializable{

    private long id;

    private String name;

    private String description;

    //@JsonProperty()
    //private Date creatingDate;

    private String imagePath;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }


    public String getPhotoPath() {
        return imagePath;
    }

    private Category category;

    public Category getCategory() {
        return category;
    }

    private User administrator;

    public User getAdministrator() {
        return administrator;
    }

    public void setAdministrator(User administrator) {
        this.administrator = administrator;
    }

    public Group(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Group(String name, String description) {
        this.name = name;
        this.description = description;
    }

    private List<User> members;

    public List<User> getMembers() {
        return members;
    }

    private List<Event> events;

    public List<Event> getEvents() {
        return events;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private long adminId;

    public long getAdminId(){return adminId;}
}
