package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.GroupRVAdapter;
import rest.letsmeet.org.letsmeetapp.busevent.NewGroupEvent;
import rest.letsmeet.org.letsmeetapp.model.Group;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.USER_ID;


public class AllGroupsFragment extends Fragment {
    long categoryID;
    long userID;

    RecyclerView rv ;
    LinearLayoutManager llm;
    GroupRVAdapter rva;

    public AllGroupsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            categoryID = extras.getLong("categoryID");
            userID = extras.getLong(USER_ID);
        }else{
            categoryID = -1;
            userID = -1;
        }
        //Toast.makeText(getContext(), "all:userID: " + userID, Toast.LENGTH_SHORT).show();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.group_fragment, container, false);

        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv);
        rv.setLayoutManager(llm);

        if(categoryID != -1){
            getGroupList(categoryID);
        }
        else{
            Log.d("AllGroupsF", "nie przekazano id kategorii");
        }

        return rootView;
    }

    private void getGroupList(long categoryID) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<Group>> call = service.getGroups(categoryID);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<Group>>() {
                @Override
                public void onResponse(Call<List<Group>> call, Response<List<Group>> response) {

                    if(response.code() == 200){
                            showGroups(response.body(), userID);
                    }
                }

                @Override
                public void onFailure(Call<List<Group>> call, Throwable t) {
                    Log.d("getGroupList", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getGroupList",e.getMessage());
        }
    }

    private void showGroups(List<Group> groups, long userId) {
        rva = new GroupRVAdapter(getContext(),groups, userId, false);
        rv.setAdapter(rva);
    }

    @Override
    public void onDetach() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDetach();
    }

    @Override
    public void onResume() {
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onResume();
    }

    @Subscribe
    public void onEvent(NewGroupEvent event){
        Log.d("EVENT", "NewGroup");
        getGroupList(categoryID);
    }

}
