package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;

import org.greenrobot.eventbus.EventBus;

import static rest.letsmeet.org.letsmeetapp.constant.Constants.PARTICIPATION_PENDING;

public class ParticipantFragment_Pending extends ParticipantFragment_Accepted{

    public ParticipantFragment_Pending(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        status = PARTICIPATION_PENDING;
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

}
