package rest.letsmeet.org.letsmeetapp.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import rest.letsmeet.org.letsmeetapp.R;
import rest.letsmeet.org.letsmeetapp.adapter.UsersRVAdapter;
import rest.letsmeet.org.letsmeetapp.model.User;
import rest.letsmeet.org.letsmeetapp.network.ApiClient;
import rest.letsmeet.org.letsmeetapp.service.APIService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UsersFragment extends Fragment {
    private long user_id;
    private long group_id;

    RecyclerView rv ;
    LinearLayoutManager llm;
    UsersRVAdapter rva;

    public UsersFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null) {
            group_id = extras.getLong("groupID");
            user_id = extras.getLong("userID");
        }else{
            group_id = -1;
            user_id = -1;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.users_fragment, container, false);

        llm = new LinearLayoutManager(getActivity());
        rv = (RecyclerView)rootView.findViewById(R.id.rv_users);
        rv.setLayoutManager(llm);

        if(group_id != -1){
            getUserList(group_id);
        }
        else{
            //Toast.makeText(getActivity(), "nie przekazano id grupy", Toast.LENGTH_LONG).show();
        }

        return rootView;
    }

    private void getUserList(long groupId) {
        try{
            APIService service = ApiClient.getRetrofit().create(APIService.class);
            Call<List<User>> call = service.getUsers(groupId);

            Log.d(String.valueOf(Log.INFO), call.request().url().toString());

            call.enqueue(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                    if(response.code() == 200){
                        if(response.body() != null){
                            showUsers(response.body());
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    Log.d("getUserList", "onFailure");
                }
            });
        }catch (Exception e){
            Log.e("Error in getUserList",e.getMessage());
        }
    }

    private void showUsers(List<User> users) {
        rva = new UsersRVAdapter(getContext(), users);
        rv.setAdapter(rva);
    }
}
